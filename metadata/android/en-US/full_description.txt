Did you ever wonder what the world looks like through a 
holomorphic function? 

No? Wondering what a 'holomorphic function' is in the first place? 

Don't worry! You don't need to know any of this to use the app. 
Just touch the screen and see the math at work! 

The Poleidoscope shows a rectangular domain of the plane of complex numbers. 
The user sets up roots and poles of a rational function on the screen 
and this function is then used to deflect the live camera picture. 

Holomorphic functions are examples of so-called 'conformal mappings' which 
are prominently featured in the works of contemporary artists such 
as M.C. Escher. The Poleidoscope provides a live experience of similar 
pictures. 

For those more involved in mathematics the app is a way to visualize 
complex polynomials and rational functions and give a feeling for 
several topics in complex analysis such as differentials of holomorphic 
functions, Rouche's theorem, ramifications, local normal forms, etc. 
