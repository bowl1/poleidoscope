/*
 *     Poleidoscope
 *     Copyright (C) 2021  Matthias Zach
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.example.poleidoscope;

import android.util.Log;

import java.util.Vector;

public class Polynomial {
    public Vector<ComplexDouble> coeff;
    public Vector<ComplexDouble> roots;
    public int degree;

    public Polynomial(){
        coeff = new Vector<ComplexDouble>();
        roots = new Vector<ComplexDouble>();
        degree = 0;
    }

    public int addRoot( ComplexDouble z ){
        roots.add( z );
        //Log.d( "Polynomial", "adding root at " + z );
        degree++;
        //updateCoeff();
        coeff.add(degree-1, new ComplexDouble((1.0)));
        int j;
        for( j= degree -1; j>0; j-- ){
            coeff.set(j,coeff.get(j-1).sub(z.mul(coeff.get(j))));
        }
        coeff.set(0, coeff.get(0).mul(z).mul(-1));
        return 0;
    }

    public int updateCoeff(){
        // calculate the coefficients from the roots
        degree = roots.size();
        coeff.clear();
        coeff.setSize( degree );
        int i,j;
        for( i=0; i<degree; i++ ){
            coeff.set(i, new ComplexDouble(1.0));
            //Log.d( "updateCoeff", "i = " + i );
            for( j=i; j>0; j-- ){
                //Log.d( "updateCoeff", "j = " + i );
                coeff.set(j, coeff.get(j-1).sub(roots.get(i).mul(coeff.get(j))));
                //Log.d( "updateCoeff", "result: " + coeff.get(j));
            }
            //Log.d( "updateCoeff", "result: " + coeff );

            coeff.set(0, coeff.get(0).mul(roots.get(i)).mul(-1.0));
        }
        /*
        coeff.clear();
        int i;
        int sign = -1;
        degree = roots.size();
        coeff.setSize(degree);
        //Log.d( "Polynomial", "call to updateCoeff; updating to a polynomial of degree " + degree );

        for( i=1; i<=degree; i++ ){
            coeff.set( degree-i, symmPol( roots, i ).muli(sign));
            sign = sign*(-1);
            //Log.d( "Polynomial", "Updated the " + i + "-th coefficient to " + coeff.get(degree-i));
            //Log.d( "Polynomial", "intermediate coefficients: " + coeff );
        }
        //Log.d( "Polynomial", "final coefficients: " + coeff );

         */
        return 0;
    }

    public int updateRoots(){
        // approximate the roots for the given coefficients
        return 0;
    }

    public ComplexDouble evalFromCoeffAt( ComplexDouble z ){
        // evaluate the Polynomial at the point z from the coefficients
        ComplexDouble y = new ComplexDouble(1.0);
        int i;
        for( i=1; i<=degree; i++ ){
            y = y.muli( z );
            y.addi( coeff.get(degree-i) );
        }
        return y;
    }

    public ComplexDouble evalFromRootsAt( ComplexDouble z ){
        // evaluate the Polynomial at the point z from the roots
        if( degree == 0 )
            return new ComplexDouble(1.0 );
        return evalRec(0, degree-1, z );

        //ComplexDouble w = z.sub(1).mul(z.sub(new ComplexDouble(0,2)));
        //return evalRec(0, degree-1, z ).div(w);
    }

    private ComplexDouble symmPol( Vector<ComplexDouble> lambda, int k ){
        //Log.d( "Polynomial", "call to symmPol with args " + lambda + " and " + k );
        // compute the k-th symmetric polynomial in lambda
        if( lambda == null ){
            //Log.d( "Polynomial", "call with empty list" );

            if( k == 0 ){
                return new ComplexDouble( 1.0 );
            } else {
                return new ComplexDouble(0.0);
            }
        }
        int n = lambda.size();
        if( n < k ) {
            //Log.d( "Polynomial", "range out of bounds above" );
            return new ComplexDouble(0.0);
        }
        if( k < 0 ) {
            //Log.d( "Polynomial", "range out of bounds below" );
            return new ComplexDouble(0.0);
        }
        if( k == 0 ) {
            //Log.d( "Polynomial", "call for the zeroeth polynomial" );
            return new ComplexDouble(1.0);
        }
        if( n == 1 ) {
            //Log.d( "Polynomial", "final return with value " + lambda.get(0) );
            return new ComplexDouble(lambda.get(0).real, lambda.get(0).imag );
        }
        Vector<ComplexDouble> truncation = new Vector<ComplexDouble>(lambda);
        truncation.remove(0);
        //Log.d( "Polynomial", "going into recursion with truncated list " + truncation );
        return (symmPol( truncation, k )).add( lambda.get(0).mul( symmPol( truncation, k-1 )));
    }

    private ComplexDouble evalRec( int i, int j, ComplexDouble z ){
        //Log.d("evalRec", "routine called with parameters " + i + ", " + j + ", " + z );
        if( i == j ){
            //Log.d( "evalRec", "reached final stage for i = j = " + i );
            return z.sub( roots.get(i) );
        } else {
            return (evalRec(i, (i+j)/2, z )).mul(evalRec( (i+j)/2+1, j, z ));
        }
    }

    public ComplexDouble evalDerivAt( int k, ComplexDouble z ){
        // evaluate the k-th derivative at z
        //Log.d( "Polynomial.evalDerivAt", "call to routine" );
        if( k == 0 )
            return evalFromRootsAt( z );
        if( k > degree )
            return new ComplexDouble( 0.0 );

        double factor = 1.0;
        int j;
        for( j=0; j<k; j++ ){
            factor*= degree-j;
        }
        //Log.d( "Polynomial.evalDerivAt", "factor = " + factor );

        ComplexDouble y = new ComplexDouble(factor);
        int i;
        for( i=degree-1; i>=k; i-- ){
            //Log.d( "Polynomial.evalDerivAt", "i = " + i );
            y = y.muli( z );
            factor = 1.0;
            for( j=0; j<k; j++ ){
                factor*= i-j;
            }
            //Log.d( "Polynomial.evalDerivAt", "factor = " + factor );
            y.addi( coeff.get(i).mul(factor) );
        }
        return y;
    }

}
