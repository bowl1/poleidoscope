/*
 *     Poleidoscope
 *     Copyright (C) 2021  Matthias Zach
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.example.poleidoscope;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Environment;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Size;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import java.io.File;
import java.util.Arrays;
import java.util.Vector;

public class PreViewOpenGL extends GLSurfaceView  {
//public class PreViewOpenGL extends View  {

    private final String TAG = "PreViewOpenGL";

    public static Context context;
    private Vector<Nipple> zeroes;
    private Vector<Nipple> poles;
    private CoordinateSystem coordSys;

    /* Variables associated to interaction with user */
    private boolean zoom = false;
    private boolean move = false;
    private float pointerDownX, pointerDownY;
    private float moveStartX, moveStartY;
    private float moveStartX1, moveStartY1;
    private static final float ZOOM_TOLERANCE = 0.1f;
    public static final float MOVE_TOLERANCE = 20.0f;
    private int width;
    private int height;

    /* Variables associated with rendering */
    private RecursiveTiling tiling;
    //public static final int MESH_GRANULARITY = 64;

    public PreViewOpenGL(Context context) {
        super(context);
        this.context = context;

        /*
        zeroes = new Vector<Nipple>();
        poles = new Vector<Nipple>();
        leadCoeff = new ComplexDouble(1.0);

         */

        // Create an OpenGL ES 2.0 context
        setEGLContextClientVersion(2);
    }

    public PreViewOpenGL(Context context, AttributeSet attrs) {
        super(context, attrs);
        Log.d("PreViewOpenGL", "constructor called" );
        this.context = context;

        /*
        zeroes = new Vector<Nipple>();
        poles = new Vector<Nipple>();
        leadCoeff = new ComplexDouble(1.0);*/

        // Create an OpenGL ES 2.0 context
        setEGLContextClientVersion(2);
    }

    public void invalidation(){
        String TAG = "PreViewOpenGL";
        Log.d( TAG, "invalidate() called" );
        tiling.setFunction(
                MainActivity.nippleModView.prepareNumerator(),
                MainActivity.nippleModView.prepareDenominator(),
                MainActivity.nippleModView.prepareLeadCoeff() );
        tiling.tile();
        tiling.prepareForRendering();

        Log.d( TAG, "preparation for rendering done" );
        requestRender();
        //MainActivity.sourceLayout.removeView(MainActivity.nippleModView);
        return;
    }

    public void requestTiling(){
        String TAG = "PreViewOpenGL";
        Log.d( TAG, "requestTiling() called" );
        tiling.setFunction(
                MainActivity.nippleModView.prepareNumerator(),
                MainActivity.nippleModView.prepareDenominator(),
                MainActivity.nippleModView.prepareLeadCoeff() );
        tiling.tile();
        tiling.prepareForRendering();
    }

    public CoordinateSystem getCoordSys(){
        return coordSys;
    }


    /*
     * Interaction section starts here
     *
     * Implementation of all methods for interacting with the user
     */
    public boolean onTouchEvent(MotionEvent event ){
        Log.d("SourceViewOpenGL", "onTouchEvent called" );
        float x = event.getX();
        float y = event.getY();
        float dx, dy;
        int i;

        switch( event.getAction() & MotionEvent.ACTION_MASK ) {
            case MotionEvent.ACTION_DOWN:
                pointerDownX = x;
                pointerDownY = y;
                switch (event.getPointerCount()) {
                    case 1:
                        Log.d("SourceViewOpenGL", "single pointer down");
                        initMovement(x, y);
                        break;
                    case 2:
                        Log.d("SourceViewOpenGL", "second pointer down by count");
                        zoomInit(event.getX(0), event.getY(0), event.getX(1), event.getY(1));
                        zoom = true;
                        break;
                    default:
                        break;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                switch (event.getPointerCount()) {
                    case 1:
                        if (!zoom) {
                            moveCoordSys(x, y);
                        }
                        break;
                    case 2:
                        zoomMove(event.getX(0), event.getY(0), event.getX(1), event.getY(1));
                        break;
                    default:
                        break;
                }
                break;
            case MotionEvent.ACTION_UP:
                if( zoom || move ) {
                    zoom = false;
                    move = false;
                } else {
                    // Set a new nipple in the touched position
                    MainActivity.nippleModView.setNewNipple( x, y );
                }
                break;

            case MotionEvent.ACTION_POINTER_DOWN:
                Log.d("SourceViewOpenGL", "second pointer down");
                zoomInit(event.getX(0), event.getY(0), event.getX(1), event.getY(1));
                zoom = true;
                break;
            case MotionEvent.ACTION_POINTER_UP:
                //drawPreimageFast(MainActivity.targetImage, new CoordinateSystem(MainActivity.targetImage.getWidth(), MainActivity.targetImage.getHeight(), -2, 2));
                break;
            default:
                break;

        }
        //MainActivity.nippleModView.updateNipples();
        //invalidation();
        return true;
    }

    /*
    public void updateNipples(){
        int i;
        Nipple nipple;
        ComplexDouble z;
        int degree = numerator.degree;
        if( degree != zeroes.size() ) {
            Log.d( "updateNipples:", "grosses Problem" );
            return;
        }
        for( i=0; i<degree; i++ ){
            z = numerator.roots.get(i);
            nipple = zeroes.get(i);
            nipple.setX(coordSys.CplxToX(z)-Nipple.BUTTON_SIZE/2);
            nipple.setY(coordSys.CplxToY(z)-Nipple.BUTTON_SIZE/2);
        }
        return;
    }

     */

    protected void initMovement( float x, float y ){
        Log.d( "initMovement", "routine called " );
        moveStartX = x;
        moveStartY = y;
        return;
    }

    protected void moveCoordSys( float x, float y ){
        if( !move ){
            // Check whether threshold for movement was triggered
            if( (Math.abs(x- pointerDownX) > MOVE_TOLERANCE ) || (Math.abs(y - pointerDownY ) > MOVE_TOLERANCE )) {
                Log.d("moveCoordSys", "movement triggered");
                move = true;
            } else {
                Log.d("moveCoordSys", "movement not sufficient");
                return;
            }
        }
        Log.d( "moveCoordSys", "routine called " );
        float dx = x - moveStartX;
        float dy = y - moveStartY;
        moveStartX = x;
        moveStartY = y;

        Log.d( "moveCoordSys", "dx = " + dx + ", dy = " + dy  );

        float x0 = (coordSys.x1-coordSys.x0)*dx/coordSys.width;
        float x1 = (coordSys.x1-coordSys.x0)*dx/coordSys.width;
        float y0 = (coordSys.y1-coordSys.y0)*dy/coordSys.height;
        float y1 = (coordSys.y1-coordSys.y0)*dy/coordSys.height;

        coordSys.x0 -= x0;
        coordSys.x1 -= x1;
        coordSys.y0 -= y0;
        coordSys.y1 -= y1;

        Log.d( "moveCoordSys", "new specifications for the window:" + coordSys.x0 + ", " + coordSys.x1 + " and " + coordSys.y0 + ", " + coordSys.y1 );

        MainActivity.nippleModView.updateNipples();
        invalidation();
        return;
    }

    protected void moveRoots( float x, float y){
        Log.d( "moveRoots", "routine called " );
        float dx = x - moveStartX;
        float dy = y - moveStartY;
        moveStartX = x;
        moveStartY = y;

        Nipple nipple;
        int i;
        for(i=0; i< zeroes.size(); i++ ){
            nipple = zeroes.get(i);
            if( nipple.selected ){
                nipple.setX( nipple.getX() + dx );
                nipple.setY( nipple.getY() + dy );
                nipple.invalidate();
            }
        }
        return;
    }

    protected void zoomInit( float x0, float y0, float x1, float y1 ){
        Log.d( "zoomInit", "routine called with parameters " + x0 + ", " + y0 + ", " + x1 + ", " + y1 );
        moveStartX = x0;
        moveStartY = y0;
        moveStartX1 = x1;
        moveStartY1 = y1;
    }

    protected void zoomMove( float x0, float y0, float x1, float y1 ){
        Log.d( "zoomMove", "routine called with parameters " + x0 + ", " + y0 + ", " + x1 + ", " + y1 );
        float d = Math.abs(moveStartX - moveStartX1) + Math.abs(moveStartY - moveStartY1 );
        float e = Math.abs(x0-x1) + Math.abs(y0-y1);

        Log.d( "zoomMove", "calculating zoom factor: " + d + ", " + e );

        if( e < ZOOM_TOLERANCE )
            e = ZOOM_TOLERANCE;
        d /= e;
        Log.d( "zoomMove", "zoom factor: " + d );


        moveStartX = x0;
        moveStartY = y0;
        moveStartX1 = x1;
        moveStartY1 = y1;

        // Update the nipples. Override the argument variables for that.
        x0 = coordSys.x0;
        x1 = coordSys.x1;
        y0 = coordSys.y0;
        y1 = coordSys.y1;
        coordSys.x0 = 0.5f*(x0*(1+d) + x1*(1-d));
        coordSys.x1 = 0.5f*(x1*(1+d) + x0*(1-d));
        coordSys.y0 = 0.5f*(y0*(1+d) + y1*(1-d));
        coordSys.y1 = 0.5f*(y1*(1+d) + y0*(1-d));

        Log.d( "zoomView", "new specifications for the window:" + coordSys.x0 + ", " + coordSys.x1 + " and " + coordSys.y0 + ", " + coordSys.y1 );

        MainActivity.nippleModView.updateNipples();
        invalidation();

        return;
    }

    /*
     * End of interaction section
     */

    /*
     * Rendering section
     */

    /* This method was used for debugging purposes.
     * It draws the triangles of the tiling without content.
     */
    @Override
    public void onDraw( Canvas canvas ){
        //tiling.setFunction(numerator, denominator, leadCoeff);
        tiling.tile();
        tiling.prepareForRendering();
        tiling.draw( canvas );
        return;
    }

    @Override
    protected void onSizeChanged( int w, int h, int oldw, int oldh ){
        super.onSizeChanged( w, h, oldw, oldh );
        Log.d( TAG, "onSizeChanged called" );

        width = w;
        height = h;
        coordSys = new CoordinateSystem( w, h );
        if( tiling == null ) {
            tiling = new RecursiveTiling(coordSys);
            setRenderer(tiling);
            Log.d( TAG, "renderer set" );


            setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
            //setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
        }
        if( Nipple.selectedNipple != null ){
            Nipple.selectedNipple.setBackgroundResource( R.drawable.red_nipple_unselected);
            Nipple.selectedNipple = null;
        }

        return;
    }

    /*
     * Camera related section
     */

    // Camera related variables

    private String cameraId;
    private Size imageDimension;
    private static final int REQUEST_CAMERA_PERMISSION = 200;
    public static final int NO_CAMERA = -1;
    protected CameraDevice cameraDevice;
    protected CaptureRequest.Builder captureRequestBuilder;
    protected CameraCaptureSession cameraCaptureSessions;
    protected TextureView textureView;
    protected boolean cameraOpen = false;

    public void switchToCamera( final int cameraId ) {
        queueEvent(new Runnable() {
            @Override
            public void run() {
                tiling.switchToCamera2(cameraId);
            }
        });
        return;
    }


    private final CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(CameraDevice camera) {
            //This is called when the camera is open
            cameraOpen = true;
            Log.e( "stateCallback", "onOpened");
            cameraDevice = camera;
            tiling.switchToCamera( cameraDevice );
        }
        @Override
        public void onDisconnected(CameraDevice camera) {
            switchToCamera( NO_CAMERA );
        }
        @Override
        public void onError(CameraDevice camera, int error) {
            switchToCamera( NO_CAMERA );
        }
    };



    public void startCamera(){
        String TAG = "startCamera";
        if( cameraOpen == false ) {
            //textureView = (TextureView) MainActivity.sourceLayout.findViewById(R.id.textureView);
            //Log.d( "startCamera", "textureView: " + textureView );
            //textureView.setSurfaceTextureListener(textureListener);
            openCamera();

            // start running the rendering process based on this camera
            Log.d(TAG, "cameraDevice = " + cameraDevice);
            cameraOpen = true;
        } else {
            Log.d( TAG, "cameraSurface = " + tiling.getCameraSurface() );
            if( tiling.getCameraSurface() != null ){
                createCameraPreview( tiling.getCameraSurface() );
            }
            Log.d( TAG, "cameraTexture = " + tiling.getCameraTexture() );
        }

        return;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void openCamera() {
        CameraManager manager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
        Log.e( "openCamera", "is camera open");
        try {
            cameraId = manager.getCameraIdList()[0];
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            assert map != null;
            imageDimension = map.getOutputSizes(SurfaceTexture.class)[0];
            // Add permission for camera and let user grant the permission
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ) {
                ActivityCompat.requestPermissions(MainActivity.self, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);
                return;
            }
            manager.openCamera(cameraId, stateCallback, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        Log.e("openCamera", "openCamera XY");
    }


    public void createCameraPreview( ) {
        try {
            //textureView = findViewById(R.id.textureView);
            assert textureView!= null;
            SurfaceTexture texture = textureView.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(imageDimension.getWidth(), imageDimension.getHeight());
            Surface surface = new Surface(texture);
            Log.d( "createCameraPreview", "building the CaptureRequest" );
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(surface);
            captureRequestBuilder.set( CaptureRequest.JPEG_THUMBNAIL_SIZE, new Size( (int) coordSys.width, (int) coordSys.width) );
            cameraDevice.createCaptureSession(Arrays.asList(surface), new CameraCaptureSession.StateCallback(){
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    //The camera is already closed
                    if (null == cameraDevice) {
                        return;
                    }
                    // When the session is ready, we start displaying the preview.
                    Log.d( "createCameraPreview", "capture session ready" );
                    cameraCaptureSessions = cameraCaptureSession;
                    updatePreview();
                }
                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                    Toast.makeText( MainActivity.self, "Configuration change", Toast.LENGTH_SHORT).show();
                }
            }, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        Log.d( "createCameraPreview", "done" );
    }

    public void createCameraPreview( Surface target ) {
        String TAG = "createCameraPreviewII";
        try {
            Log.d( TAG, "building the CaptureRequest" );
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(target);
            cameraDevice.createCaptureSession(Arrays.asList(target), new CameraCaptureSession.StateCallback(){
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    //The camera is already closed
                    if (null == cameraDevice) {
                        return;
                    }
                    // When the session is ready, we start displaying the preview.
                    Log.d( "onConfigured", "capture session ready" );
                    cameraCaptureSessions = cameraCaptureSession;
                    updatePreview();
                }
                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                    Toast.makeText( MainActivity.self, "Configuration change", Toast.LENGTH_SHORT).show();
                }
            }, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        Log.d( TAG, "done" );
    }

    protected void updatePreview() {
        String TAG = "updatePreview";
        if(null == cameraDevice) {
            Log.e("updatePreview", "updatePreview error, return");
        }
        Log.d( TAG, "updating..." );
        captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
        try {
            cameraCaptureSessions.setRepeatingRequest(captureRequestBuilder.build(), null, null);
            Log.d( TAG, "success" );
        } catch (CameraAccessException e) {
            Log.d( TAG, "failure" );
            e.printStackTrace();
        }
        Log.d( TAG, "finished updating" );
    }

    public String takeSnapshotOld(){
        requestRender();
        Log.d( "Snapshot", "event queued" );
        final String filename = Environment.getExternalStorageDirectory().toString()
                + File.separator + Environment.DIRECTORY_PICTURES
                + File.separator + "Poleidoscope" + (SystemClock.uptimeMillis() % 10000) + ".jpeg";
        queueEvent(new Runnable() {
            @Override
            public void run() {
                tiling.takeSnapshotOld( filename );
            }
        });
        return( filename );
    }

    public void takeSnapshot(){
        tiling.recordingGif = RecursiveTiling.TAKE_SNAPSHOT;
        return;
    }

    public void startRecording(){
        tiling.recordingGif = RecursiveTiling.START_RECORDING;
        return;
    }

    public void stopRecording(){
        tiling.recordingGif = RecursiveTiling.STOP_RECORDING;
        return;
    }

    public void reset(){
        coordSys = new CoordinateSystem( width, height );
        tiling.setCoordSys( coordSys );
        return;
    }
}
