/*
 *     Poleidoscope
 *     Copyright (C) 2021  Matthias Zach
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.example.poleidoscope;

import android.util.Log;

public class CoordinateSystem {
    public float x0, x1, y0, y1;
    public float width, height;

    public CoordinateSystem( float w, float h ){
        width = w;
        height = h;
        x0 = (float) -1;
        x1 = (float) 1;
        y0 = -(height/width)*x0;
        y1 = -(height/width)*x1;
        Log.i("CoordinateSystem", "Coordinate system for screen with width " + width + " and height " + height + " set up");
        Log.i("CoordinateSystem", "View comprises the ranges " + x0 + " to " + x1 + " and " + y0 + " to " + y1 );
    }

    public CoordinateSystem( float w, float h, float xStart, float xEnd ){
        width = w;
        height = h;
        x0 = (float) xStart;
        x1 = (float) xEnd;
        y0 = -(height/width)*x0;
        y1 = -(height/width)*x1;
        Log.i("CoordinateSystem", "Coordinate system for screen with width " + width + " and height " + height + " set up");
        Log.i("CoordinateSystem", "View comprises the ranges " + x0 + " to " + x1 + " and " + y0 + " to " + y1 );
    }

    public ComplexDouble screenToCplx(float x, float y ) {
        return new ComplexDouble( (double) x0 + x*(x1-x0)/width, (double) y0 + y*(y1-y0)/height );
    }

    public float CplxToX( ComplexDouble z ){
        return (float) (width * (z.real - x0) / (x1 - x0));
    }

    public float CplxToY( ComplexDouble z ){
        return (float) (height * (z.imag - y0) / (y1 - y0));
    }
}
