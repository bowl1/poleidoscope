/*
 *     Poleidoscope
 *     Copyright (C) 2021  Matthias Zach
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.example.poleidoscope;

import android.content.Context;
import android.graphics.Canvas;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;

public class Nipple
        extends androidx.appcompat.widget.AppCompatButton
        implements View.OnTouchListener
{

    public boolean selected = false;
    public static final int BUTTON_SIZE = 64;

    private CoordinateSystem coordSys;
    private ComplexDouble value;
    private static ActionMode nippleModMode = null;
    public static Nipple selectedNipple = null;

    public int type;
    public static final int ROOT = 0;
    public static final int POLE = 1;
    public static final int COEFFICIENT = 2;

    public Nipple(Context context) {
        super(context);
        ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams( ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);
        super.setLayoutParams( params );
        setOnTouchListener(this);
    }

    public Nipple(Context context, CoordinateSystem coordSys, float x, float y) {
        super(context);
        //ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams( ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);
        ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams( BUTTON_SIZE,  BUTTON_SIZE );
        super.setLayoutParams( params );
        this.coordSys = coordSys;
        value = coordSys.screenToCplx(x,y);
        setX(x-BUTTON_SIZE/2);
        setY(y-BUTTON_SIZE/2);
        //setBackgroundColor(Color.BLUE);
        setBackgroundResource(R.drawable.red_nipple_unselected);
        setOnTouchListener(this);
    }

    public Nipple(Context context, CoordinateSystem coordSys, ComplexDouble z ) {
        super(context);
        //ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams( ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);
        ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams( BUTTON_SIZE,  BUTTON_SIZE );
        super.setLayoutParams( params );
        this.coordSys = coordSys;
        value = z;
        setX( coordSys.CplxToX(z)-BUTTON_SIZE/2);
        setY( coordSys.CplxToY(z)-BUTTON_SIZE/2);
        //setBackgroundColor(Color.BLUE);
        setBackgroundResource(R.drawable.red_nipple_unselected);
        setOnTouchListener(this);
    }

    public Nipple(Context context, AttributeSet attrs) {
        super(context, attrs);
        ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams( ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);
        super.setLayoutParams( params );
        setOnTouchListener(this);
    }

    public Nipple(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams( ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);
        super.setLayoutParams( params );
        setOnTouchListener(this);
    }

    /*
    @Override
    public void onClick( View v ){
        Log.d( "onClick", "clicked" );
        //setSelected( !selected );
        nippleModMode = MainActivity.self.startActionMode( nippleModCallback );
        //MainActivity.refreshSourceLayout();
        return;
    }
     */

    @Override
    protected void onDraw( Canvas canvas ){
        super.onDraw(canvas);
    }

    public void updateFromScreenValues(){
        value = coordSys.screenToCplx( getX()+BUTTON_SIZE/2, getY()+BUTTON_SIZE/2 );
        if( nippleModMode != null ) {
            nippleModMode.setTitle(String.format("\t%.2f + %.2f i", value.real, value.imag));
        }
        return;
    }

    public void updateFromCoordValues(){
        setX( coordSys.CplxToX( value ) - BUTTON_SIZE/2 );
        setY( coordSys.CplxToY( value ) - BUTTON_SIZE/2 );
        //this.invalidate();
        return;
    }

    public void setSelected( boolean selection ){
        selected = selection;
        if (selected) {
            //setBackgroundColor( Color.RED );
            setBackgroundResource(R.drawable.red_nipple_selected);
        } else {
            //setBackgroundColor( Color.BLUE );
            setBackgroundResource(R.drawable.red_nipple_unselected);
        }
        invalidate();
        return;
    }

    public ComplexDouble getValue(){
        return value;
    }


    public ActionMode.Callback nippleModCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            if( selectedNipple != null ){
                Toast.makeText(MainActivity.self, "you already selected another nipple", Toast.LENGTH_SHORT ).show();
                selectedNipple.setBackgroundResource(R.drawable.red_nipple_selected);
                return false;
            }
            selectedNipple = Nipple.this;
            nippleModMode = mode;
            setBackgroundResource(R.drawable.red_nipple_selected);
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate( R.menu.nipple_mod_menu, menu );
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            MainActivity.nippleModView.activate();
            mode.setTitle( String.format( "\t%.2f + %.2f i", value.real, value.imag ));
            return true;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch( item.getItemId() ){
                case R.id.delete_nipple:
                    Toast.makeText(MainActivity.self, "deleted", Toast.LENGTH_SHORT ).show();
                    selectedNipple = null;
                    MainActivity.nippleModView.deleteNipple(Nipple.this);
                    mode.finish();
                    MainActivity.sourceView.invalidation();
                    return true;
                case R.id.toggle_nipple:
                    //Toast.makeText(MainActivity.self, "toggling this nipple", Toast.LENGTH_SHORT ).show();
                    if( type == Nipple.ROOT ){
                        type = Nipple.POLE;
                        mode.setTitle("switched to pole");
                    } else {
                        type = Nipple.ROOT;
                        mode.setTitle("switched to root");
                    }
                    MainActivity.sourceView.invalidation();
                    return true;
                default:
                    break;
            }
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            nippleModMode = null;
            selectedNipple = null;
            setBackgroundResource(R.drawable.red_nipple_unselected);
            //MainActivity.sourceLayout.removeView(nippleModView);
            MainActivity.nippleModView.deactivate();
            Log.d("onDestroyActionMode", "deactivating action mode" );
            return;
        }
    };

    private float xStart, yStart;
    private long touchStart;
    private boolean move = false;
    private final float TOLERANCE = 20f;
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        String TAG = "onTouch";
        switch( event.getAction() ){
            case MotionEvent.ACTION_DOWN:
                Log.d( TAG, "action down at " + event.getX() + ", " + event.getY() );
                touchStart = SystemClock.uptimeMillis();
                xStart = event.getX();
                yStart = event.getY();
                return true;
            case MotionEvent.ACTION_MOVE:
                float x = event.getX();
                float y = event.getY();
                Log.d( TAG, "action move at " + x + ", " + y );
                if(  !move ) {
                    if (Math.abs(x - xStart) > TOLERANCE || Math.abs(y - yStart) > TOLERANCE)
                        move = true;
                } else {
                    setX(getX() + x - BUTTON_SIZE / 2);
                    setY(getY() + y - BUTTON_SIZE / 2);
                    updateFromScreenValues();
                    MainActivity.sourceView.invalidation();
                }
                return true;
            case MotionEvent.ACTION_UP:
                Log.d( TAG, "action up" );
                if( move ){
                    move = false;

                    return true;
                }
                if( SystemClock.uptimeMillis() - touchStart > 500 ){
                    /*
                    // delete this nipple
                    MainActivity.nippleModView.deleteNipple(this);
                    MainActivity.sourceView.invalidation();
                    Toast.makeText(MainActivity.self, "deleted", Toast.LENGTH_SHORT).show();
                     */
                    nippleModMode = MainActivity.self.startActionMode( nippleModCallback );

                    return true;
                }
                if( type == ROOT ){
                    type = POLE;
                    setBackgroundResource(R.drawable.green_nipple_unselected);
                    Toast.makeText(MainActivity.self, "switched to pole", Toast.LENGTH_SHORT).show();
                } else {
                    type = ROOT;
                    setBackgroundResource(R.drawable.red_nipple_unselected);
                    Toast.makeText(MainActivity.self, "switched to root", Toast.LENGTH_SHORT).show();
                }
                MainActivity.sourceView.invalidation();
                return true;
        }
        return false;
    }
}
