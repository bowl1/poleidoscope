/*
 *     Poleidoscope
 *     Copyright (C) 2021  Matthias Zach
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.example.poleidoscope;

public class ComplexDouble {
    public double real, imag;

    public ComplexDouble(){
        real = imag =0;
        return;
    }

    public ComplexDouble( double a ){
        real = a;
        imag = 0;
        return;
    }

    public ComplexDouble( double a, double b ){
        real = a;
        imag = b;
        return;
    }

    public ComplexDouble( ComplexDouble w ){
        real = w.real;
        imag = w.imag;
        return;
    }

    public ComplexDouble add( ComplexDouble w ){
        return( new ComplexDouble( real + w.real, imag + w.imag));
    }

    public ComplexDouble addi( ComplexDouble w ){
        real +=w.real;
        imag += w.imag;
        return(this);
    }

    public ComplexDouble add( double a ){
        return( new ComplexDouble( real + a, imag));
    }

    public ComplexDouble addi( double a ){
        real +=a;
        return(this);
    }

    public ComplexDouble sub( ComplexDouble w ){
        return( new ComplexDouble( real - w.real, imag - w.imag));
    }

    public ComplexDouble subi( ComplexDouble w ){
        real -=w.real;
        imag -=w.imag;
        return(this);
    }

    public ComplexDouble sub( double a ){
        return( new ComplexDouble( real - a, imag));
    }

    public ComplexDouble subi( double a ){
        real -=a;
        return(this);
    }

    public ComplexDouble mul( ComplexDouble w ){
        return( new ComplexDouble( real *w.real - imag *w.imag, real *w.imag + imag *w.real));
    }

    public ComplexDouble muli( ComplexDouble w ){
        double t = real;
        real = real *w.real - imag *w.imag;
        imag = t*w.imag + w.real * imag;
        return(this);
    }

    public ComplexDouble mul( double a ){
        return( new ComplexDouble( a* real, a* imag));
    }

    public ComplexDouble muli( double a ){
        real *=a;
        imag *=a;
        return(this);
    }

    public ComplexDouble div( ComplexDouble w ){
        double r = w.real *w.real + w.imag *w.imag;
        return( new ComplexDouble( (real *w.real + imag *w.imag)/r, (imag *w.real - real *w.imag)/r ));
    }

    public ComplexDouble divi( ComplexDouble w ){
        double r = w.real *w.real + w.imag *w.imag;
        double t = real;
        real = (real *w.real + imag *w.imag)/r;
        imag = (imag *w.real - t*w.imag)/r;
        return( this );
    }

    public ComplexDouble div( double a ){
        return( new ComplexDouble( real/a, imag/a ));
    }

    public ComplexDouble divi( double a ){
        real /= a;
        imag /= a;
        return( this );
    }

    public String toString(){
        return( "( " + real + ", " + imag + " )" );
    }

    public double manhattanAbs(){
        return Math.abs(real) + Math.abs(imag);
    }

    public double abs(){
        return Math.sqrt( real*real + imag*imag );
    }
    public double absSquared(){
        return real*real + imag*imag;
    }
}
