/*
 *     Poleidoscope
 *     Copyright (C) 2021  Matthias Zach
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.example.poleidoscope;

import android.util.Log;

public class Node {
    public Node branch00, branch01, branch10, branch11;
    public short index;
    public ComplexDouble[] g;

    public Node(){
        branch00 = branch01 = branch10 = branch11 = null;
        index = -1; // Indicate that this node has not been filled with content yet
        g = new ComplexDouble[2]; // Allocate memory for the content
    }

    public Node(short index, ComplexDouble[] g ){
        branch00 = branch01 = branch10 = branch11 = null;
        this.index = index;
        this.g = new ComplexDouble[2];
        this.g[0] = new ComplexDouble(g[0]);
        this.g[1] = new ComplexDouble( g[1] );
        return;
    }

    public Node( Node node ){
        this.index = node.index;
        if( node.g != null ) {
            g = new ComplexDouble[2];
            g[0] = new ComplexDouble(node.g[0]);
            g[1] = new ComplexDouble(node.g[1]);
        } else {
            g = null;
        }
        return;
    }

    public Node lookup( int i, int j, int k ){
        //Log.d( "lookup", "method called with parameters " + i + ", " + j + ", " + k );
        //if( i == 0 && j == 0 ){
        if( k == 0 ){
            return this;
        }
        if( branch00 == null ){
            //Log.d( "lookup", "invalid branch" );
            return null;
        }
        if( ( i >> (k-1) ) % 2 == 0 ){
            if( (j >> (k-1)) % 2 == 0 ){
                return branch00.lookup( i % (1 << (k-1)), j % (1<<(k-1)), k-1 );
            } else {
                return branch01.lookup( i % (1 << (k-1)), j % (1<<(k-1)), k-1 );
            }
        } else {
            if( (j>>(k-1)) % 2 == 0 ){
                return branch10.lookup( i % (1 << (k-1)), j % (1<<(k-1)), k-1 );
            } else {
                return branch11.lookup( i % (1 << (k-1)), j % (1<<(k-1)), k-1 );
            }
        }
    }

    public void split(){
        branch00 = new Node(this);
        branch01 = new Node();
        branch10 = new Node();
        branch11 = new Node();
        return;
    }
}
