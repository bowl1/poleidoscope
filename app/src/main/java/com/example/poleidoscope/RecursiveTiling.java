/*
 *     Poleidoscope
 *     Copyright (C) 2021  Matthias Zach
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.example.poleidoscope;

import android.Manifest;
import android.app.ActionBar;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RecordingCanvas;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.os.Build;
import android.os.Environment;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.util.Log;
import android.util.Size;
import android.view.Surface;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Vector;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import static com.example.poleidoscope.PreViewOpenGL.NO_CAMERA;

public class RecursiveTiling implements GLSurfaceView.Renderer {
//public class RecursiveTiling  {

    private Vector<ComplexDouble[]> vertexList;
    private Vector<short[]> triangleList;
    private Node root;
    private volatile Polynomial numerator;
    private volatile Polynomial denominator;
    private volatile ComplexDouble leadCoeff;
    private CoordinateSystem coordSys;
    private short vertexCount;
    private int triangleCount;

    private volatile FloatBuffer vertexBuffer;
    private volatile short vertexBufferLength;
    private volatile ShortBuffer triangleBuffer;
    private volatile int triangleBufferLength;
    private volatile boolean readyToRender = false;
    private volatile boolean rendering = false;
    private float imageAspectRatio = (float) 1.0;

    // Specifies the maximal deviation of the approximated picture
    // in a cell from the original picture
    private static double TOLERANCE = 3;
    private static final int vertexCountMax = 12000;
    private double tilingConstant;
    private double t = 0;


    // Varbiables necessary for recording GIFs
    public static final int RECORDING_PAUSED = 0;
    public static final int START_RECORDING = 1;
    public static final int RECORDING = 2;
    public static final int STOP_RECORDING = 3;
    public static final int TAKE_SNAPSHOT = 4;
    public volatile int recordingGif = RECORDING_PAUSED;
    // private AnimatedGifEncoder encoder;
    private ByteArrayOutputStream bos;
    private ArrayList<Bitmap> frames;
    private long[] timeStamps;
    private int framecount;
    private int qualityDecrease = 4;

    public RecursiveTiling( CoordinateSystem coordSys ){
        root = new Node();
        vertexList = new Vector<ComplexDouble[]>();
        triangleList = new Vector<short[]>();
        numerator = new Polynomial();
        denominator = new Polynomial();
        leadCoeff = new ComplexDouble(1.0, 3.0 );
        this.coordSys = coordSys;
        return;
    }

    public void tile( ){
        String TAG = "tile";
        root = new Node();
        vertexList = new Vector<ComplexDouble[]>();
        triangleList = new Vector<short[]>();

        vertexCount = 0;
        triangleCount = 0;

        ComplexDouble z;
        ComplexDouble w;

        z = new ComplexDouble( coordSys.x0, coordSys.y0 );
        w = numerator.evalFromRootsAt(z).div(denominator.evalFromRootsAt(z)).div(leadCoeff);
        ComplexDouble[] g00 = new ComplexDouble[2];
        g00[0] = z;
        g00[1] = w;
        vertexList.add(g00);
        root.index = 0;
        root.g = g00;
        root.split();

        z = new ComplexDouble( coordSys.x0, coordSys.y1 );
        w = numerator.evalFromRootsAt(z).div(denominator.evalFromRootsAt(z)).div(leadCoeff);
        ComplexDouble[] g01 = new ComplexDouble[2];
        g01[0] = z;
        g01[1] = w;
        vertexList.add(g01);
        root.branch01.index = 1;
        root.branch01.g = g01;

        z = new ComplexDouble( coordSys.x1, coordSys.y0 );
        w = numerator.evalFromRootsAt(z).div(denominator.evalFromRootsAt(z)).div(leadCoeff);
        ComplexDouble[] g10 = new ComplexDouble[2];
        g10[0] = z;
        g10[1] = w;
        vertexList.add(g10);
        root.branch10.index = 2;
        root.branch10.g = g10;

        z = new ComplexDouble( coordSys.x1, coordSys.y1 );
        w = numerator.evalFromRootsAt(z).div(denominator.evalFromRootsAt(z)).div(leadCoeff);
        ComplexDouble[] g11 = new ComplexDouble[2];
        g11[0] = z;
        g11[1] = w;
        vertexList.add(g11);
        root.branch11.index = 3;
        root.branch11.g = g11;

        vertexCount=4;

        //Log.d( "tile", "initial values for tiling set to " + root.branch00.g[0] + root.branch10.g[0] + root.branch01.g[0] + root.branch11.g[0] );
        //Log.d( "tile", "testing lookup" + root.branch00 + " vs. " + root.lookup(0,0, 1 ));
        //Log.d( "tile", "testing lookup" + root.branch10 + " vs. " + root.lookup(1,0, 1 ));
        //Log.d( "tile", "testing lookup" + root.branch01 + " vs. " + root.lookup(0,1, 1 ));
        //Log.d( "tile", "testing lookup" + root.branch11 + " vs. " + root.lookup(1,1, 1 ));

        tilingConstant =  8* TOLERANCE * TOLERANCE
                *((coordSys.x1- coordSys.x0)*(coordSys.x1- coordSys.x0)
                + (coordSys.y1 - coordSys.y0)*(coordSys.y1 - coordSys.y0))
                /(coordSys.width*coordSys.width+coordSys.height*coordSys.height);
        divideRecursively( 0,0,1 );
    }

    private void divideRecursively( int i, int j, int k ){
        //Log.d( "divideRecursively", "call to method with parameters " + i + ", " + j + ", " + k );
        Node v00 = root.lookup( i, j, k );
        Node v01 = root.lookup( i, j+1, k );
        Node v10 = root.lookup( i+1, j, k );
        Node v11 = root.lookup( i+1, j+1, k );
        assert v00 != null;
        assert v10 != null;
        assert v01 != null;
        assert v11 != null;
        //Log.d( "tile", "initial values for tiling set to " + v00.g[0] + v10.g[0] + v01.g[0] + v11.g[0] );


        if (needsMoreTiling(v00.g, v01.g, v10.g, v11.g)) {

            //Log.d( "divideRecursively", "this square needs more tiling" );
            // Look up whether the vertices have been computed already
            if( v00.branch00 == null ) {
                //Log.d( "divideRecursively", "splitting the v00 node" );
                v00.split();
            }

            ComplexDouble z,w;
            ComplexDouble[] g = new ComplexDouble[2];

            //Compute the central vertex
            g[0] = (v00.g[0].add(v11.g[0])).div(2);
            g[1] = numerator.evalFromRootsAt(g[0]).div(denominator.evalFromRootsAt(g[0])).div(leadCoeff);

            v00.branch11.g = g;
            vertexList.add(g);
            v00.branch11.index = vertexCount;
            v00.branch11.g = g;
            vertexCount++;

            // Look up the vertex to the right
            if( v00.branch10.index == -1 ) {
                // Compute the vertex to the right
                g = new ComplexDouble[2];
                g[0] = (v00.g[0].add(v10.g[0])).div(2);
                g[1] = numerator.evalFromRootsAt(g[0]).div(denominator.evalFromRootsAt(g[0])).div(leadCoeff);
                v00.branch10.g = g;
                vertexList.add(g);
                v00.branch10.index = vertexCount;
                v00.branch10.g = g;
                vertexCount++;
            }

            // Look up the vertex below
            if( v00.branch01.index == -1 ) {
                // Compute the vertex to the right
                g = new ComplexDouble[2];
                g[0] = (v00.g[0].add(v01.g[0])).div(2);
                g[1] = numerator.evalFromRootsAt(g[0]).div(denominator.evalFromRootsAt(g[0])).div(leadCoeff);
                v00.branch01.g = g;
                vertexList.add(g);
                v00.branch01.index = vertexCount;
                v00.branch01.g = g;
                vertexCount++;
            }

            if( v10.branch00 == null ) {
                //Log.d( "divideRecursively", "splitting the v10 node" );
                v10.split();
            }

            // Look up the vertex at the center of the right side face
            if( v10.branch01.index == -1 ) {
                g = new ComplexDouble[2];
                g[0] = (v10.g[0].add(v11.g[0])).div(2);
                g[1] = numerator.evalFromRootsAt(g[0]).div(denominator.evalFromRootsAt(g[0])).div(leadCoeff);
                v10.branch01.g = g;
                vertexList.add(g);
                v10.branch01.index = vertexCount;
                v10.branch01.g = g;
                vertexCount++;
            }

            if( v01.branch00 == null ) {
                //Log.d( "divideRecursively", "splitting the v01 node" );
                v01.split();
            }

            // Look up the vertex at the center of the bottom face
            if( v01.branch10.index == -1 ) {
                g = new ComplexDouble[2];
                g[0] = (v01.g[0].add(v11.g[0])).div(2);
                g[1] = numerator.evalFromRootsAt(g[0]).div(denominator.evalFromRootsAt(g[0])).div(leadCoeff);
                v01.branch10.g = g;
                vertexList.add(g);
                v01.branch10.index = vertexCount;
                v01.branch10.g = g;
                vertexCount++;
            }

            if( v11.branch00 == null ){
                //Log.d( "divideRecursively", "splitting the v11 node" );
                v11.split();
            }

            divideRecursively( 2*i, 2*j, k+1 );
            divideRecursively( 2*i + 1, 2*j, k+1 );
            divideRecursively( 2*i, 2*j+1, k+1 );
            divideRecursively( 2*i+1, 2*j+1, k+1 );

            return;
        }

        // Set up the two triangles for this rectangle
        short[] triangle = new short[3];
        triangle[0] = v00.index;
        triangle[1] = v01.index;
        triangle[2] = v10.index;

        triangleList.add(triangle);
        triangleCount++;

        triangle = new short[3];
        triangle[0] = v01.index;
        triangle[1] = v10.index;
        triangle[2] = v11.index;

        triangleList.add(triangle);
        triangleCount++;

        return;
    }

    private boolean needsMoreTiling( ComplexDouble[] g00, ComplexDouble[] g01, ComplexDouble[] g10, ComplexDouble[] g11 ){
        //Log.d("needsMoreTiling", "call to method with parameters " + g00[0] + g10[0] + g01[0] + g11[0] );
        // Check whether the lower bound on tiling is reached
        if (Math.abs(coordSys.CplxToX(g00[0]) - coordSys.CplxToX(g11[0])) < TOLERANCE || vertexCount > 32000 ) {
            //Log.d("needsMoreTiling", "lower bound for tesselation reached" );
            return false;
        }
        ComplexDouble firstDer,secondDer;
        firstDer = (g10[1].sub(g00[1])).div(g10[0].sub(g00[0]));
        firstDer = firstDer.add( (g01[1].sub(g00[1])).div(g01[0].sub(g00[0])));
        firstDer = firstDer.add( (g11[1].sub(g10[1])).div(g11[0].sub(g10[0])));
        firstDer = firstDer.add( (g11[1].sub(g01[1])).div(g11[0].sub(g01[0])));
        //firstDer = firstDer.div(4);
        secondDer = (g00[1].mul(g10[0].sub(g01[0])))
                .add(g10[1].mul(g01[0].sub(g00[0])))
                .sub(g01[1].mul(g10[0].sub(g00[0])))
                .div(
                        g10[0].sub( g01[0])
                                .mul( g10[0].sub( g00[0] ))
                                .mul( g01[0].sub( g00[0] ))
                );
        secondDer = secondDer.add( (g11[1].mul(g10[0].sub(g01[0])))
                .add(g10[1].mul(g01[0].sub(g11[0])))
                .sub(g01[1].mul(g10[0].sub(g11[0])))
                .div(
                        g10[0].sub( g01[0])
                                .mul( g10[0].sub( g11[0] ))
                                .mul( g01[0].sub( g11[0] ))
                ));
        secondDer = secondDer.add( (g11[1].mul(g00[0].sub(g01[0])))
                .add(g00[1].mul(g01[0].sub(g11[0])))
                .sub(g01[1].mul(g00[0].sub(g11[0])))
                .div(
                        g00[0].sub( g01[0])
                                .mul( g00[0].sub( g11[0] ))
                                .mul( g01[0].sub( g11[0] ))
                ));
        secondDer = secondDer.add( (g11[1].mul(g00[0].sub(g10[0])))
                .add(g00[1].mul(g10[0].sub(g11[0])))
                .sub(g10[1].mul(g00[0].sub(g11[0])))
                .div(
                        g00[0].sub( g10[0])
                                .mul( g00[0].sub( g11[0] ))
                                .mul( g10[0].sub( g11[0] ))
                ));
        //secondDer = secondDer.div(4);
        //Log.d( "needsMoreTiling", "square of the absolute value of the first and second derivative: " + firstDer.absSquared() + ", " + secondDer.absSquared() );
        //return secondDer.manhattanAbs()*((g00[0].sub(g11[0])).absSquared()) < TOLERANCE * firstDer.manhattanAbs() * ( Math.abs(coordSys.x1- coordSys.x0) + Math.abs(coordSys.y1 - coordSys.y0));
        double dummy = (g00[0].sub(g11[0])).absSquared();
        return secondDer.absSquared()*dummy*dummy > firstDer.absSquared() * tilingConstant;
    }

    public void prepareForRendering(){
        String TAG = "prepareForRendering";
        while( rendering ){};
        readyToRender = false;

        Log.d( "prepareForRendering", "vertexCount: " + vertexCount + " vs. " + vertexList.size());
        Log.d( "prepareForRendering", "triangleCount: " + triangleCount + " vs. " + triangleList.size());
        Log.d( "prepareForRendering", "imageAspectRatio: " + imageAspectRatio );
        if( cameraTexture != null ){
            float[] mat = new float[16];
            cameraTexture.getTransformMatrix( mat );
            Log.d( TAG, String.format( "transformation Matrix:\n %f %f %f %f\n %f %f %f %f\n %f %f %f %f\n %f %f %f %f", mat[0], mat[1], mat[2], mat[3], mat[4], mat[5], mat[6], mat[7], mat[8], mat[9], mat[10], mat[11], mat[12], mat[13], mat[14], mat[15] ));
        } else {
            Log.d(TAG, "cameraTexture is null");
        }


        /* Write out the vertex list for the OpenGL renderer */
        vertexBuffer = ByteBuffer.allocateDirect(vertexCount*5*4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        vertexBuffer.position(0);

        // Prepare for translation to the OpenGL coordinate system
        double dx = coordSys.x1 - coordSys.x0;
        double dy = coordSys.y0 - coordSys.y1;
        double x,y,z;
        float[] vertex = new float[5];
        ComplexDouble[] g = new ComplexDouble[2];
        Iterator<ComplexDouble[]> i =  vertexList.iterator();
        while( i.hasNext() ){
            g = i.next();

            /*
            //x = -(2*( g[0].imag - coordSys.x0 )/dx - 1 );
            x = -(1-t/100)*(2*( g[0].imag - coordSys.x0 )/dx - 1 ) - t/100*g[1].imag;
            y = (1-t/100)*(2*( g[0].real - coordSys.y1 )/dy - 1 );
            z = g[1].real;

            vertex[0] = (float)( x - 0.3*y -0.4*z );
            vertex[1] = (float)( y + 0.3* z );
            vertex[2] = (float)( z-0.2*y + 0.4*x );

             */

            // The standard frustum shows the ranges from -1 to 1 in x and y
            vertex[0] = (float) (2*( g[0].real - coordSys.x0 )/dx - 1 );
            vertex[1] = (float) (2*( g[0].imag - coordSys.y1 )/dy - 1 );
            vertex[2] = 0.0f;

            vertex[3] = (float)(0.5*(g[1].real+1));
            //vertex[4] = 0.5f*(((float)g[1].imag*imageAspectRatio+1));
            vertex[4] = 1.0f - 0.5f*(((float)g[1].imag*imageAspectRatio+1));
            //vertex[3] = (float)(0.25*(g[1].real+2));
            //vertex[4] = 1.0f - (float)(0.25*(g[1].imag+2));
            /*Log.d( "prepareForRendering", "rendered vertex: " + vertex[0] + ", "
                    + vertex[1] + ", "
                    + vertex[2] + ", "
                    + vertex[3] + ", "
                    + vertex[4] + ", "
            );*/
            vertexBuffer.put(vertex,0,5);
        }

        /* Write out the triangle list for the OpenGL renderer */
        triangleBuffer = ByteBuffer.allocateDirect(triangleCount*4*3).order(ByteOrder.nativeOrder()).asShortBuffer();
        triangleBuffer.position(0);

        short[] triangle = new short[3];
        Iterator<short[]> j = triangleList.iterator();
        while( j.hasNext() ){
            triangle = j.next();
            triangleBuffer.put( triangle, 0, 3 );
        }

        vertexBufferLength = vertexCount;
        triangleBufferLength = triangleCount;

        // Adjust the tolerance acoording to how many triangles there are
        if( vertexCount > vertexCountMax ){
            TOLERANCE = TOLERANCE*1.1;
            Log.d( TAG, String.format("vertexCount is %d, adjusting tolerance to %f", vertexCount, TOLERANCE ) );
        } else if (vertexCount < vertexCountMax-1000 && TOLERANCE > 1 ){
            TOLERANCE = TOLERANCE*0.9;
            Log.d( TAG, String.format("vertexCount is %d, adjusting tolerance to %f", vertexCount, TOLERANCE ) );
        }
        readyToRender = true;
        return;
    }

    public void draw0( Canvas canvas ){

        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.MITER);
        paint.setStrokeWidth(4f);

        // Only draw the shapes of the tiling on the canvas
        //Iterator<ComplexDouble[]> i = vertexList.iterator();
        Iterator<short[]> t = triangleList.iterator();
        ComplexDouble z1,z2,z0;
        short[] triangle;
        while(t.hasNext()){
            triangle = t.next();
            // Draw the shape of the triangle on the screen
            z0 = vertexList.get(triangle[0])[0];
            z1 = vertexList.get(triangle[1])[0];
            z2 = vertexList.get(triangle[2])[0];

            canvas.drawLine( coordSys.CplxToX(z0), coordSys.CplxToY(z0), coordSys.CplxToX(z1), coordSys.CplxToY(z1), paint );
            canvas.drawLine( coordSys.CplxToX(z1), coordSys.CplxToY(z1), coordSys.CplxToX(z2), coordSys.CplxToY(z2), paint );
            canvas.drawLine( coordSys.CplxToX(z0), coordSys.CplxToY(z0), coordSys.CplxToX(z2), coordSys.CplxToY(z2), paint );
        }
        return;
    }

    public void draw( Canvas canvas ){

        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.MITER);
        paint.setStrokeWidth(4f);
        Bitmap bitmap = BitmapFactory.decodeResource(PreViewOpenGL.context.getResources(), R.drawable.grid_cropped );


        // Only draw the shapes of the tiling on the canvas
        int i;
        short p,q,r;
        float x0,y0,x1,y1;
        for( i=0; i<triangleCount; i++ ) {
            p = triangleBuffer.get(3 * i);
            q = triangleBuffer.get(3 * i + 1);
            r = triangleBuffer.get(3 * i + 2);

            x0 = (vertexBuffer.get(5 * p) + 1) / 2 * coordSys.width;
            x1 = (vertexBuffer.get(5 * q) + 1) / 2 * coordSys.width;
            y0 = (1 - vertexBuffer.get(5 * p + 1)) / 2 * coordSys.height;
            y1 = (1 - vertexBuffer.get(5 * q + 1)) / 2 * coordSys.height;
            canvas.drawLine(x0, y0, x1, y1, paint);

            x0 = (vertexBuffer.get(5 * q) + 1) / 2 * coordSys.width;
            x1 = (vertexBuffer.get(5 * r) + 1) / 2 * coordSys.width;
            y0 = (1 - vertexBuffer.get(5 * q + 1)) / 2 * coordSys.height;
            y1 = (1 - vertexBuffer.get(5 * r + 1)) / 2 * coordSys.height;
            canvas.drawLine(x0, y0, x1, y1, paint);

            x0 = (vertexBuffer.get(5 * p) + 1) / 2 * coordSys.width;
            x1 = (vertexBuffer.get(5 * r) + 1) / 2 * coordSys.width;
            y0 = (1 - vertexBuffer.get(5 * p + 1)) / 2 * coordSys.height;
            y1 = (1 - vertexBuffer.get(5 * r + 1)) / 2 * coordSys.height;
            canvas.drawLine(x0, y0, x1, y1, paint);

        }
        return;
    }

    public void setFunction(Polynomial numerator, Polynomial denominator, ComplexDouble leadCoeff ){
        this.numerator = numerator;
        this.denominator = denominator;
        this.leadCoeff = leadCoeff;
        /*
        String TAG = "setFunction";
        if( numerator == null ){
            this.numerator = new Polynomial();
        } else {
            // make a deep copy of the given polynomial
            int i;
            this.numerator.roots.clear();
            for (i = 0; i < numerator.roots.size(); i++) {
                this.numerator.roots.add(new ComplexDouble(numerator.roots.get(i)));
            }
            this.numerator.updateCoeff();
        }

        if( denominator == null ){
            this.denominator = new Polynomial();
        } else {
            // make a deep copy of the given polynomial
            int i;
            this.denominator.roots.clear();
            for (i = 0; i < denominator.roots.size(); i++) {
                this.denominator.roots.add(new ComplexDouble(denominator.roots.get(i)));
            }
            this.denominator.updateCoeff();
        }

        if( leadCoeff == null ){
            this.leadCoeff = new ComplexDouble(1.0 );
        } else {
            this.leadCoeff = new ComplexDouble( leadCoeff );
        }

        Log.d( TAG, "numerator: " + numerator.roots );
        Log.d( TAG, "denominator: " + denominator.roots );
        Log.d( TAG, "leadCoeff: " + leadCoeff );

         */
        return;
    }

    /*
     * Rendering section.
     */

    public static final String vertexShaderCode =
            //"uniform mat4 u_MVPMatrix;      \n"		            // A constant representing the combined model/view/projection matrix.

              "attribute vec4 a_Position;     \n"		// Per-vertex position information we will pass in.
            + "attribute vec2 a_texCoord;     \n"       // The texture coordinate attribute
            + "varying vec2 v_texCoord;       \n"       // This will be passed into the fragment shader
            + "void main()                    \n"		// The entry point for our vertex shader.
            + "{                              \n"
            //+ "   v_Color = a_Color;          \n"		// Pass the color through to the fragment shader.
            // It will be interpolated across the triangle.
            + "   v_texCoord = a_texCoord;    \n"       // Pass the texture coordinate on
            + "   gl_Position = a_Position;   \n" 	    // gl_Position is a special variable used to store the final position.
            + "}                              \n";      // normalized screen coordinates.

    public static final String fragmentShaderCode =
              "precision mediump float;       \n"		// Set the default precision to medium. We don't need as high of a
            // precision in the fragment shader.
            + "uniform sampler2D u_texture;   \n"       // The input texture
            + "varying vec2 v_texCoord;       \n"       // The texture coordinate
            // triangle per fragment.
            + "void main()                    \n"		// The entry point for our fragment shader.
            + "{                              \n"
            + "   gl_FragColor = texture2D( u_texture, v_texCoord );     \n"		// Evaluate the texture
            + "}                              \n";

    public static final String fragmentShaderCodeCamera =
              "#extension GL_OES_EGL_image_external : require\n"
                      + "precision mediump float;       \n"		// Set the default precision to medium. We don't need as high of a
                      + "uniform samplerExternalOES u_texture;\n"
            + "uniform mat2 u_tex_transform; \n"
            // precision in the fragment shader.
            + "varying vec2 v_texCoord;       \n"
                      + "vec2 dummy;\n"// The texture coordinate
            // triangle per fragment.
            + "void main()                    \n"		// The entry point for our fragment shader.
            + "{   "
            + "dummy = vec2(v_texCoord[1],1.0-v_texCoord[0]);\n"
            + "   gl_FragColor = texture2D( u_texture, dummy );     \n"		// Evaluate the texture
            + "}                              \n";

    private int programHandle;
    private int positionHandle;
    private int textureUniformHandle;
    private int textureCoordinateHandle;
    private int textureDataHandle;
    private int textureTransformationHandle;

    private float[] textureTransformation;

    private int vertexShaderHandle;
    private int fragmentShaderHandle;
    private int fragmentShaderHandleCamera;
    private volatile boolean useCameraForRendering = false;
    private volatile SurfaceTexture cameraTexture;
    private volatile Surface cameraSurface;
    private volatile boolean switchToCameraFlag = false;

    private boolean cameraOpen = false;
    private static final int REQUEST_CAMERA_PERMISSION = 200;
    protected CameraDevice cameraDevice;
    protected CaptureRequest.Builder captureRequestBuilder;
    protected CameraCaptureSession cameraCaptureSessions;
    private Size imageDimension;

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        String TAG = "onSurfaceCreated";

        // Set the background clear color to gray.
        GLES20.glClearColor(0.5f, 0.5f, 0.5f, 0.5f);


        // Load in the vertex shader.
        vertexShaderHandle = GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER);

        if (vertexShaderHandle != 0)
        {
            // Pass in the shader source.
            GLES20.glShaderSource(vertexShaderHandle, vertexShaderCode );

            // Compile the shader.
            GLES20.glCompileShader(vertexShaderHandle);

            // Get the compilation status.
            final int[] compileStatus = new int[1];
            GLES20.glGetShaderiv(vertexShaderHandle, GLES20.GL_COMPILE_STATUS, compileStatus, 0);

            // If the compilation failed, delete the shader.
            if (compileStatus[0] == 0)
            {
                GLES20.glDeleteShader(vertexShaderHandle);
                vertexShaderHandle = 0;
            }
        }

        if (vertexShaderHandle == 0)
        {
            throw new RuntimeException("Error creating vertex shader.");
        }

        // Load in the fragment shader.
        fragmentShaderHandle = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);

        if (fragmentShaderHandle != 0)
        {
            // Pass in the shader source.
            GLES20.glShaderSource(fragmentShaderHandle, fragmentShaderCode );

            // Compile the shader.
            GLES20.glCompileShader(fragmentShaderHandle);

            // Get the compilation status.
            final int[] compileStatus = new int[1];
            GLES20.glGetShaderiv(fragmentShaderHandle, GLES20.GL_COMPILE_STATUS, compileStatus, 0);

            // If the compilation failed, delete the shader.
            if (compileStatus[0] == 0)
            {
                GLES20.glDeleteShader(fragmentShaderHandle);
                fragmentShaderHandle = 0;
            }
        }

        if (fragmentShaderHandle == 0)
        {
            throw new RuntimeException("Error creating fragment shader.");
        }

        // Load in the fragment shader for camera use
        fragmentShaderHandleCamera = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);

        if (fragmentShaderHandleCamera != 0)
        {
            // Pass in the shader source.
            GLES20.glShaderSource(fragmentShaderHandleCamera, fragmentShaderCodeCamera );

            // Compile the shader.
            GLES20.glCompileShader(fragmentShaderHandleCamera);

            // Get the compilation status.
            final int[] compileStatus = new int[1];
            GLES20.glGetShaderiv(fragmentShaderHandleCamera, GLES20.GL_COMPILE_STATUS, compileStatus, 0);

            // If the compilation failed, delete the shader.
            if (compileStatus[0] == 0)
            {
                Log.d( TAG, "Error compiling the fragment shader for the camera; output " + compileStatus[0] );
                Log.d( TAG, GLES20.glGetShaderInfoLog(fragmentShaderHandleCamera));
                GLES20.glDeleteShader(fragmentShaderHandleCamera);
                fragmentShaderHandleCamera = 0;
            }
        }

        if (fragmentShaderHandleCamera == 0)
        {
            throw new RuntimeException("Error creating fragment shader for the camera.");
        }

        // Create a program object and store the handle to it.
        programHandle = GLES20.glCreateProgram();

        if (programHandle != 0)
        {
            // Bind the vertex shader to the program.
            GLES20.glAttachShader(programHandle, vertexShaderHandle);

            // Bind the fragment shader to the program.
            GLES20.glAttachShader(programHandle, fragmentShaderHandle);

            // Bind attributes
            GLES20.glBindAttribLocation(programHandle, 0, "a_Position");
            GLES20.glBindAttribLocation(programHandle, 1, "a_Color");
            GLES20.glBindAttribLocation(programHandle, 2, "a_texCoord");


            // Link the two shaders together into a program.
            GLES20.glLinkProgram(programHandle);

            // Get the link status.
            final int[] linkStatus = new int[1];
            GLES20.glGetProgramiv(programHandle, GLES20.GL_LINK_STATUS, linkStatus, 0);

            // If the link failed, delete the program.
            if (linkStatus[0] == 0)
            {
                GLES20.glDeleteProgram(programHandle);
                programHandle = 0;
            }
        }

        if (programHandle == 0)
        {
            throw new RuntimeException("Error creating program.");
        }

        positionHandle = GLES20.glGetAttribLocation(programHandle, "a_Position");
        textureUniformHandle = GLES20.glGetUniformLocation(programHandle, "u_texture");
        textureCoordinateHandle = GLES20.glGetAttribLocation(programHandle, "a_texCoord");
        Log.d( TAG, "positionHandle = " + positionHandle );
        Log.d( TAG, "textureUniformHandle = " + textureUniformHandle );
        Log.d( TAG, "textureCoordinateHandle = " + textureCoordinateHandle );

        /*
        positionHandle = GLES20.glGetAttribLocation(programHandle, "a_Position");
        textureUniformHandle = GLES20.glGetUniformLocation(programHandle, "u_texture");
        textureCoordinateHandle = GLES20.glGetAttribLocation(programHandle, "a_texCoord");

         */

        // Set up texture processing
        textureDataHandle = loadTexture( PreViewOpenGL.context, R.drawable.poleidoscope_logo );
        //textureDataHandle = loadTexture( PreViewOpenGL.context, R.drawable.circle );
        //textureDataHandle = loadTexture( PreViewOpenGL.context, R.drawable.boy );

        // Tell OpenGL to use this program when rendering.
        GLES20.glUseProgram(programHandle);



        // Set up a startup triangle
        int vertexCount = 4;
        vertexBuffer = ByteBuffer.allocateDirect(vertexCount * 5 * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        vertexBuffer.position(0);
        float[] vertex = new float[5];
        float scale = 0.5f;
        vertex[0] = scale * (float) -1;
        vertex[1] = scale * (float) -1*coordSys.width/coordSys.height;
        vertex[2] = (float) 0.0;
        vertex[3] = (float) 0.0;
        vertex[4] = (float) 1.0;
        vertexBuffer.put(vertex, 0, 5);
        vertex[0] = scale * (float) -1;
        vertex[1] = scale * (float) 1*coordSys.width/coordSys.height;
        vertex[2] = (float) 0.0;
        vertex[3] = (float) 0.0;
        vertex[4] = (float) 0;
        vertexBuffer.put(vertex, 0, 5);
        vertex[0] = scale * (float) 1;
        vertex[1] = scale * (float) -1*coordSys.width/coordSys.height;
        vertex[2] = scale * (float) 0.0;
        vertex[3] = (float) 1;
        vertex[4] = (float) 1;
        vertexBuffer.put(vertex, 0, 5);
        vertex[0] = scale * (float) 1;
        vertex[1] = scale * (float) 1*coordSys.width/coordSys.height;
        vertex[2] = scale * (float) 0.0;
        vertex[3] = (float) 1;
        vertex[4] = (float) 0;
        vertexBuffer.put(vertex, 0, 5);

        triangleBuffer = ByteBuffer.allocateDirect(2 * 4 * 3).order(ByteOrder.nativeOrder()).asShortBuffer();
        triangleBuffer.position(0);

        short[] triangle = new short[3];
        triangle[0] = 0;
        triangle[1] = 1;
        triangle[2] = 2;
        triangleBuffer.put(triangle, 0, 3);

        triangle[0] = 3;
        triangle[1] = 2;
        triangle[2] = 1;
        triangleBuffer.put(triangle, 0, 3);

        triangleBufferLength=2;
        readyToRender = true;
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        GLES20.glViewport(0, 0, width, height);
    }

    public void switchToCamera2( int cameraNumber ){
        String TAG = "switchToCamera";
        if( cameraOpen ){
            cameraDevice.close();
            cameraOpen = false;
        }
        if( cameraNumber >=0 ) {
            Log.d( TAG, "starting the camera" );
            try {
                // try to open the camera
                if (ActivityCompat.checkSelfPermission(MainActivity.self, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ) {
                    ActivityCompat.requestPermissions(
                            MainActivity.self,
                            new String[]{
                                    Manifest.permission.CAMERA,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                            },
                            REQUEST_CAMERA_PERMISSION
                    );
                    return;
                }
                // Open the camera on the backgroundthread stored in the
                // main activity for this purpose
                MainActivity.cameraManager.openCamera(
                        MainActivity.cameraManager.getCameraIdList()[cameraNumber],
                        stateCallback,
                        MainActivity.cameraHandler
                );

                // Determine the buffer size and aspect ratio of the camera image
                // most suitable for our purposes
                CameraCharacteristics characteristics = MainActivity.cameraManager.getCameraCharacteristics(
                        MainActivity.cameraManager.getCameraIdList()[cameraNumber] );
                StreamConfigurationMap map = characteristics.get(
                        CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                assert map != null;

                //Log.d( "openCamera", map.toString() );
                Size[] outputSizes = map.getOutputSizes(SurfaceTexture.class);
                for( int i = 0; i<outputSizes.length; i++ ){
                    imageDimension = outputSizes[i];
                    Log.d( "openCamera", "i = " + i + ", size: " + imageDimension );
                    Log.d( "openCamera", "output supported: " + map.isOutputSupportedFor(SurfaceTexture.class));

                    if( imageDimension.getWidth() <= 1.6*coordSys.width ){
                        Log.d( "openCamera", "i= " + i );
                        break;
                    }
                }
                Log.e("openCamera", MainActivity.cameraManager.getCameraIdList()[cameraNumber] );
                Log.d( "openCamera", "preview size: " + imageDimension );
                imageAspectRatio = (float) imageDimension.getHeight() / (float) imageDimension.getWidth();
                Log.d( "openCamera", "imageAspectRatio: " + imageAspectRatio );
            } catch( CameraAccessException e ){
                e.printStackTrace();
            }
            useCameraForRendering = true;
        }

        if( cameraNumber == NO_CAMERA ){
            Log.d( TAG, "switching to no camera" );

            GLES20.glDeleteProgram( programHandle );

            programHandle = GLES20.glCreateProgram();
            if( programHandle == 0 ) {
                throw new RuntimeException("Failed to create OpenGL program");
            }

            GLES20.glAttachShader( programHandle, vertexShaderHandle );
            GLES20.glAttachShader( programHandle, fragmentShaderHandle );

            // Bind attributes
            GLES20.glBindAttribLocation(programHandle, 0, "a_Position");
            GLES20.glBindAttribLocation(programHandle, 1, "a_Color");
            GLES20.glBindAttribLocation(programHandle, 2, "a_texCoord");

            GLES20.glLinkProgram(programHandle);
            // Get the link status.
            final int[] linkStatus = new int[1];
            GLES20.glGetProgramiv(programHandle, GLES20.GL_LINK_STATUS, linkStatus, 0);

            // If the link failed, delete the program.
            if (linkStatus[0] == 0)
            {
                GLES20.glDeleteProgram(programHandle);
                programHandle = 0;
                throw new RuntimeException("Linking the OpenGL program failed");
            }

            positionHandle = GLES20.glGetAttribLocation(programHandle, "a_Position");
            Log.d( TAG, "positionHandle = " + positionHandle );
            textureUniformHandle = GLES20.glGetUniformLocation(programHandle, "u_texture");
            Log.d( TAG, "textureUniformHandle = " + textureUniformHandle );
            textureCoordinateHandle = GLES20.glGetAttribLocation(programHandle, "a_texCoord");
            Log.d( TAG, "textureCoordinateHandle = " + textureCoordinateHandle );

            // Load the standard texture
            //textureDataHandle = loadTexture( PreViewOpenGL.context, R.drawable.circle );
            textureDataHandle = loadTexture( PreViewOpenGL.context, R.drawable.grid_cropped );

            GLES20.glUseProgram(programHandle);

            useCameraForRendering = false;
            imageAspectRatio = (float) 1.0;
            Log.d( TAG, "switching to no camera succeeded." );
        }
        setFunction(
                MainActivity.nippleModView.prepareNumerator(),
                MainActivity.nippleModView.prepareDenominator(),
                MainActivity.nippleModView.prepareLeadCoeff() );
        tile();
        prepareForRendering();
        return;
    }

    private final CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened( final CameraDevice camera ) {
            MainActivity.sourceView.queueEvent(new Runnable() {
                @Override
                public void run() {
                    String TAG = "onOpened";
                    //This is called when the camera is open
                    cameraOpen = true;
                    Log.e( "stateCallback", "onOpened");
                    cameraDevice = camera;

                    GLES20.glDeleteProgram(programHandle);

                    programHandle = GLES20.glCreateProgram();
                    if( programHandle == 0 ){
                        throw new RuntimeException( "Error creating program for OpenGL" );
                    }
                    GLES20.glAttachShader( programHandle, vertexShaderHandle );
                    GLES20.glAttachShader( programHandle, fragmentShaderHandleCamera );

                    // Bind attributes
                    GLES20.glBindAttribLocation(programHandle, 0, "a_Position");
                    GLES20.glBindAttribLocation(programHandle, 1, "a_Color");
                    GLES20.glBindAttribLocation(programHandle, 2, "a_texCoord");

                    // Link the two shaders together into a program.
                    Log.d( TAG, "linking the shaders" );
                    GLES20.glLinkProgram(programHandle);

                    // Get the link status.
                    final int[] linkStatus = new int[1];
                    GLES20.glGetProgramiv(programHandle, GLES20.GL_LINK_STATUS, linkStatus, 0);

                    // If the link failed, delete the program.
                    if (linkStatus[0] == 0)
                    {
                        GLES20.glDeleteProgram(programHandle);
                        Log.d( TAG, "linking the program failed" );
                        Log.d( TAG, "link status: " + linkStatus[0] );
                        programHandle = 0;
                    } else {
                        Log.d( TAG, "link status: " + linkStatus[0] );
                    }

                    positionHandle = GLES20.glGetAttribLocation(programHandle, "a_Position");
                    Log.d( TAG, "positionHandle = " + positionHandle );
                    textureUniformHandle = GLES20.glGetUniformLocation(programHandle, "u_texture");
                    Log.d( TAG, "textureUniformHandle = " + textureUniformHandle );
                    textureCoordinateHandle = GLES20.glGetAttribLocation(programHandle, "a_texCoord");
                    Log.d( TAG, "textureCoordinateHandle = " + textureCoordinateHandle );

                    // Load the texture provided by the camera
                    Log.d( TAG, "loading the texture from the camera " );
                    textureDataHandle = loadCameraTexture();
                    // set the flag for the onDrawFrame method
                    useCameraForRendering = true;
                    cameraTexture = new SurfaceTexture( textureDataHandle );
                    cameraTexture.setOnFrameAvailableListener(new SurfaceTexture.OnFrameAvailableListener() {
                        @Override
                        public void onFrameAvailable(SurfaceTexture surfaceTexture) {
                            MainActivity.sourceView.requestRender();
                        }
                    });
                    float[] mat = new float[16];
                    cameraTexture.getTransformMatrix( mat );
                    Log.d( TAG, String.format( "transformation Matrix:\n %f %f %f %f\n %f %f %f %f\n %f %f %f %f\n %f %f %f %f", mat[0], mat[1], mat[2], mat[3], mat[4], mat[5], mat[6], mat[7], mat[8], mat[9], mat[10], mat[11], mat[12], mat[13], mat[14], mat[15] ));

                    textureTransformation = new float[]{
                            1.0f, 0.0f,
                            0.0f, 1.0f
                    };
                    textureTransformationHandle = GLES20.glGetAttribLocation(programHandle, "u_tex_transform" );

                    //cameraTexture.setDefaultBufferSize( (int) (coordSys.width), (int) (coordSys.height) );
                    cameraTexture.setDefaultBufferSize( (int) (imageDimension.getWidth()), (int) (imageDimension.getHeight()) );
                    cameraSurface = new Surface(cameraTexture);
                    Log.d( TAG, "preview size: " + imageDimension );
                    cameraTexture.getTransformMatrix( mat );
                    Log.d( TAG, String.format( "transformation Matrix:\n %f %f %f %f\n %f %f %f %f\n %f %f %f %f\n %f %f %f %f", mat[0], mat[1], mat[2], mat[3], mat[4], mat[5], mat[6], mat[7], mat[8], mat[9], mat[10], mat[11], mat[12], mat[13], mat[14], mat[15] ));

                    Log.d( TAG, "done with setting up loading the texture from the camera" );
                    Log.d( TAG, "cameraSurface = " + cameraSurface );

                    //cameraTexture.updateTexImage();
                    //Log.d( TAG, "updated the image" );

                    // Tell OpenGL to use this program when rendering.
                    GLES20.glUseProgram(programHandle);
                    Log.d( TAG, "told the renderer to use the new program" );


                    /*
                     * Now that the renderer and its interface are prepared, set up the
                     * CaptureSession of the camera targeting the surface provided by the
                     * renderer
                     */
                    try {
                        Log.d( TAG, "building the CaptureRequest" );
                        captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
                        captureRequestBuilder.addTarget(cameraSurface);
                        cameraDevice.createCaptureSession(
                                Arrays.asList(cameraSurface),
                                new CameraCaptureSession.StateCallback() {
                                    @Override
                                    public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                                        String TAG = "onConfigured";
                                        //The camera is already closed
                                        if (null == cameraDevice) {
                                            return;
                                        }
                                        // When the session is ready, we start displaying the preview.
                                        Log.d("onConfigured", "capture session ready");
                                        cameraCaptureSessions = cameraCaptureSession;

                                        captureRequestBuilder.set(
                                                CaptureRequest.CONTROL_MODE,
                                                CameraMetadata.CONTROL_MODE_AUTO
                                        );
                                        try {
                                            cameraCaptureSessions.setRepeatingRequest(
                                                    captureRequestBuilder.build(),
                                                    new CameraCaptureSession.CaptureCallback() {
                                                        @Override
                                                        public void onCaptureStarted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, long timestamp, long frameNumber) {
                                                            super.onCaptureStarted(session, request, timestamp, frameNumber);
                                                        }
                                                    },
                                                    MainActivity.cameraHandler
                                            );
                                        } catch (CameraAccessException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                                        Toast.makeText(
                                                MainActivity.self,
                                                "Configuration change",
                                                Toast.LENGTH_SHORT
                                        ).show();
                                    }
                                },
                                MainActivity.cameraHandler
                        );
                        Log.d( TAG, "success setting up the camera" );
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                    return;
                }
            });
            return;
        }
        @Override
        public void onDisconnected(CameraDevice camera) {
            switchToCamera2( NO_CAMERA );
        }
        @Override
        public void onError(CameraDevice camera, int error) {
            switchToCamera2( NO_CAMERA );
        }
    };

    public Surface getCameraSurface(){
        return cameraSurface;
    }

    public SurfaceTexture getCameraTexture(){
        return cameraTexture;
    }

    public void switchToCamera( CameraDevice camera ){
        final String TAG = "switchToCamera";

        switchToCameraFlag = true;

        return;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onDrawFrame(GL10 gl) {
        String TAG = "onDrawFrame";

        while( readyToRender == false ){};
        rendering = true;


        GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT );

        // Set the active texture unit to texture unit 0.
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);

        if( useCameraForRendering == true ){
            cameraTexture.updateTexImage();
            Log.d( TAG, "using the camera texture" );
            // Use the External OES texture
            GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, textureDataHandle);
            GLES20.glUniformMatrix2fv( textureTransformationHandle, 1, false, textureTransformation, 0 );
        } else {
            // Bind the texture to this unit.
            Log.d( TAG, "using the standard texture" );
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureDataHandle);
        }

        Log.d( TAG, "positionHandle = " + positionHandle );
        Log.d( TAG, "textureUniformHandle = " + textureUniformHandle );
        Log.d( TAG, "textureCoordinateHandle = " + textureCoordinateHandle );

        // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
        GLES20.glUniform1i(textureUniformHandle, 0);

        Log.d( TAG, "mark1" );

        vertexBuffer.position(0);
        GLES20.glVertexAttribPointer(positionHandle, 3, GLES20.GL_FLOAT, false,
                5*4, vertexBuffer );
        GLES20.glEnableVertexAttribArray(positionHandle);
        Log.d( TAG, "mark2" );


        vertexBuffer.position(3);
        GLES20.glVertexAttribPointer(textureCoordinateHandle, 2 , GLES20.GL_FLOAT,
                false, 5*4 , vertexBuffer );
        GLES20.glEnableVertexAttribArray(textureCoordinateHandle);
        Log.d( TAG, "mark3" );


        triangleBuffer.position(0);
        Log.d( TAG, "length of the triangle buffer: " + triangleBuffer.remaining() );
        Log.d( TAG, "triangleBufferLength * 6: " + 6*triangleBufferLength );
        GLES20.glDrawElements( GLES20.GL_TRIANGLES, 3*triangleBufferLength, GLES20.GL_UNSIGNED_SHORT, triangleBuffer );

        Log.d( TAG, "mark4" );


        //GLES20.glDrawElements( GLES20.GL_POINTS, 5*vertexCount, GLES20.GL_FLOAT, vertexBuffer );


        GLES20.glDisableVertexAttribArray(textureCoordinateHandle);
        GLES20.glDisableVertexAttribArray(positionHandle);
        Log.d( TAG, "mark5" );

        // Do the recording if necessary
        switch( recordingGif ){
            case RECORDING_PAUSED:
                break;
            case START_RECORDING:
                frames = new ArrayList<Bitmap>();
                timeStamps = new long[512];
                framecount = 0;
                /*
                bos = new ByteArrayOutputStream();
                encoder = new AnimatedGifEncoder();
                encoder.setDelay(100);
                encoder.start(bos);
                 */
                recordingGif = RECORDING;
                break;
            case RECORDING:
                if( framecount == 512 ) {
                    // Emergency stop
                    break;
                }
                timeStamps[framecount] = SystemClock.uptimeMillis();
                Log.d( "gif recording", "time stamp: " + timeStamps[framecount] );
                if( framecount > 0 && timeStamps[framecount] - timeStamps[framecount-1] <100 ){
                    break;
                }
                // Prepare for snapping
                try {
                    int width = (int) coordSys.width;
                    int height = (int) coordSys.height;
                    Bitmap snapshot = Bitmap.createBitmap( width, height, Bitmap.Config.ARGB_8888);
                    ByteBuffer pixelBuffer = ByteBuffer.allocateDirect(width * height * 4);
                    pixelBuffer.order(ByteOrder.nativeOrder());
                    GLES20.glReadPixels( 0, 0, width, height, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, pixelBuffer);

                    // The RGB values need a complicated conversion from openGL to bitmap
                    int intPixelBuffer[] = new int[width*height];
                    pixelBuffer.asIntBuffer().get(intPixelBuffer);
                    for( int i=0; i< width*height; i++ ){
                        intPixelBuffer[i] = ((intPixelBuffer[i] & 0xff00ff00)) | ((intPixelBuffer[i] & 0x000000ff) << 16) | ((intPixelBuffer[i] & 0x00ff0000) >> 16);
                    }
                    snapshot.setPixels(intPixelBuffer, height*width - width, -width, 0, 0, width, height );
                    frames.add(snapshot);
                    framecount++;
                } catch( Exception e ){
                    Log.e( "Snapshot", "Out of memory exception" );
                    MainActivity.self.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.self);
                            dialogBuilder.setMessage( "There was an error saving the file!\n\n"+e.getMessage() );
                            dialogBuilder.setTitle("Error");
                            dialogBuilder.setNeutralButton("OK", null );
                            AlertDialog dialog = dialogBuilder.create();
                            dialog.show();
                        }
                    });
                    e.printStackTrace();
                }
                break;
            case STOP_RECORDING:
                writeGif();
                recordingGif = RECORDING_PAUSED;
                break;
            case TAKE_SNAPSHOT:
                takeSnapshot();
                frames.clear();
                framecount = 0;
                timeStamps = null;
                recordingGif = RECORDING_PAUSED;
                break;
        }

        rendering = false;
        Log.d( TAG, "rendering done" );
        return;
    }

    public int loadTexture(final Context context, final int resourceId)
    {
        Log.d("TilingRender", "call to loadTexture." );
        final int[] textureHandle = new int[1];

        GLES20.glGenTextures(1, textureHandle, 0);

        Log.d("TilingRender", "textureHandle = " + textureHandle[0] );

        if (textureHandle[0] != 0)
        {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;	// No pre-scaling

            // Read in the resource
            final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resourceId, options);

            // Bind to the texture in OpenGL
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);

            // Set filtering
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
            //GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_MIRRORED_REPEAT);
            //GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_MIRRORED_REPEAT);


            // Load the bitmap into the bound texture.
            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);

            // Recycle the bitmap, since its data has been loaded into OpenGL.
            bitmap.recycle();
        }

        if (textureHandle[0] == 0)
        {
            throw new RuntimeException("Error loading texture. Error code: " + GLES20.glGetError() );
        }

        // make the texture available to the rest of the program
        // mTextureDataHandle = textureHandle[0];
        return textureHandle[0];
    }

    public int loadCameraTexture(){
        int[] textures = new int[1];

        // Generate the texture to where android view will be rendered
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glGenTextures(1, textures, 0);
        Log.d("loadCameraTexture", "Texture generate");

        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, textures[0]);
        Log.d("loadCameraTexture", "Texture bind");

        GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GL10.GL_TEXTURE_MIN_FILTER,GL10.GL_LINEAR);
        GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GL10.GL_TEXTURE_WRAP_S, GL10.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GL10.GL_TEXTURE_WRAP_T, GL10.GL_CLAMP_TO_EDGE);

        return textures[0];
    }

    private void takeSnapshot(){
        String TAG = "new Snapshot";
        Log.d( TAG, "method called" );

        if( frames.size() == 0 ){
            Log.e( TAG, "no picture to save!" );
            return;
        }

        try {
            String filename =
                    Environment.getExternalStorageDirectory().toString()
                            + File.separator + Environment.DIRECTORY_PICTURES
                            + File.separator + "Poleidoscope" + (SystemClock.uptimeMillis() % 10000) + ".jpeg";
            FileOutputStream outStream = new FileOutputStream(filename);
            frames.get(0).compress(Bitmap.CompressFormat.JPEG, 100, outStream );
            outStream.flush();
            outStream.close();

            MediaScannerConnection.scanFile(MainActivity.self, new String[]{filename}, null, null );


            // Inform the user about the saved image
            MainActivity.self.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Snackbar snapshotMessage = Snackbar.make( MainActivity.sourceLayout, R.string.snapshot_taken, Snackbar.LENGTH_LONG );
                    snapshotMessage.setAction("View", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.parse(filename), "image/*" );
                            MainActivity.self.startActivity( intent );
                        }
                    });
                    snapshotMessage.show();
                }
            });
        } catch(Exception e) {
            e.printStackTrace();
            MainActivity.self.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.self);
                    dialogBuilder.setMessage( "There was an error saving the file!\n\n"+e.getMessage() );
                    dialogBuilder.setTitle("Error");
                    dialogBuilder.setNeutralButton("OK", null );
                    AlertDialog dialog = dialogBuilder.create();
                    dialog.show();
                }
            });
        }
        return;
    }

    public int takeSnapshotOld(String filename ){
        int width = (int) coordSys.width;
        int height = (int) coordSys.height;

        // Open the file for saving the snapshot
        OutputStream fOut = null;
        File file = new File( filename );

        try {
            fOut = new FileOutputStream(file);
        } catch( Exception e ){
            Log.e( "Snapshot", "Error opening the file" );
            e.printStackTrace();
            MainActivity.self.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.self);
                    dialogBuilder.setMessage( "There was an error saving the file!\n\n"+e.getMessage() );
                    dialogBuilder.setTitle("Error");
                    dialogBuilder.setNeutralButton("OK", null );
                    AlertDialog dialog = dialogBuilder.create();
                    dialog.show();
                }
            });
            return(-1);
        }

        // Prepare for snapping
        Bitmap snapshot = Bitmap.createBitmap( width, height, Bitmap.Config.ARGB_8888);
        try {
            ByteBuffer pixelBuffer = ByteBuffer.allocateDirect(width * height * 4);
            pixelBuffer.order(ByteOrder.nativeOrder());
            GLES20.glReadPixels( 0, 0, width, height, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, pixelBuffer);

            // The RGB values need a complicated conversion from openGL to bitmap
            int intPixelBuffer[] = new int[width*height];
            pixelBuffer.asIntBuffer().get(intPixelBuffer);
            for( int i=0; i< width*height; i++ ){
                intPixelBuffer[i] = ((intPixelBuffer[i] & 0xff00ff00)) | ((intPixelBuffer[i] & 0x000000ff) << 16) | ((intPixelBuffer[i] & 0x00ff0000) >> 16);
            }
            snapshot.setPixels(intPixelBuffer, height*width - width, -width, 0, 0, width, height );
        } catch( Exception e ){
            Log.e( "Snapshot", "Out of memory exception" );
            MainActivity.self.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.self);
                    dialogBuilder.setMessage( "Out of memory!\n\n"+e.getMessage() );
                    dialogBuilder.setTitle("Error");
                    dialogBuilder.setNeutralButton("OK", null );
                    AlertDialog dialog = dialogBuilder.create();
                    dialog.show();
                }
            });
            e.printStackTrace();
        }

        // Write the bitmap to the file
        snapshot.compress(Bitmap.CompressFormat.JPEG, 100, fOut );
        try {
            fOut.flush();
            fOut.close();
        } catch( Exception e ){
            Log.e( "Snapshot", "Error saving the file" );
            e.printStackTrace();
            MainActivity.self.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.self);
                    dialogBuilder.setMessage( "There was an error saving the file!\n\n"+e.getMessage() );
                    dialogBuilder.setTitle("Error");
                    dialogBuilder.setNeutralButton("OK", null );
                    AlertDialog dialog = dialogBuilder.create();
                    dialog.show();
                }
            });
            return(-1);
        }

        return(0);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void writeGif(){
        MainActivity.self.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final int qualityDecrease;

                SeekBar seekBar = new SeekBar(MainActivity.self);
                seekBar.setMin(1);
                seekBar.setMax(7);
                seekBar.setProgress(4);
                AlertDialog.Builder gifSaveDialogBuilder = new AlertDialog.Builder( MainActivity.self );
                gifSaveDialogBuilder.setMessage( "Select quality"
                ).setTitle(
                        "Save as GIF"
                ).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.d( "Save GIF", "picked number: " + seekBar.getProgress() );
                                //Log.d( "numberPicker", "picked number: " + numberPicker.getValue());

                                MainActivity.self.findViewById(R.id.snapshot).setVisibility(View.INVISIBLE);
                                Snackbar savingMessage = Snackbar.make( MainActivity.sourceLayout, "writing GIF to storage...", Snackbar.LENGTH_LONG );
                                savingMessage.show();
                                final int quality = 8-seekBar.getProgress();
                                Thread writeGifThread = new Thread() {
                                    @Override
                                    public void run() {
                                        super.run();
                                        String TAG = "writeGif";
                                        int width = (int) coordSys.width;
                                        int height = (int) coordSys.height;
                                        int newWidth = width/quality;
                                        int newHeight = height/quality;
                                        Bitmap snapshot;

                                        bos = new ByteArrayOutputStream();
                                        //encoder = new AnimatedGifEncoder();
                                        //encoder.setRepeat(0);
                                        //encoder.setQuality(20);
                                        //encoder.start(bos);

                                        for( int i=0; i<frames.size()-1; i++ ){
                                            int intPixelBuffer[] = new int[width*height];
                                            int outPixelBuffer[] = new int[newWidth*newHeight];
                                            frames.get(i).getPixels(intPixelBuffer, height*width - width, -width, 0, 0, width, height );
                                            for( int l=0; l<newHeight; l++ ){
                                                for( int m=0; m<newWidth; m++ ) {
                                                    outPixelBuffer[l * newWidth + m] = intPixelBuffer[l * quality * width + m * quality];
                                                }
                                            }
                                            snapshot = Bitmap.createBitmap( newWidth, newHeight, Bitmap.Config.ARGB_8888);
                                            snapshot.setPixels(outPixelBuffer, newHeight*newWidth - newWidth, -newWidth, 0, 0, newWidth, newHeight );
                                            //encoder.addFrame(snapshot);
                                            //encoder.setDelay((int) (timeStamps[i+1]-timeStamps[i]));

                                            Log.d( TAG, "adding frame no " + i + " out of " + frames.size() );
                                        }
                                        //encoder.finish();
                                        frames.clear();
                                        framecount = 0;
                                        timeStamps = null;

                                        try {
                                            String filename =
                                                    Environment.getExternalStorageDirectory().toString()
                                                            + File.separator + Environment.DIRECTORY_PICTURES
                                                            + File.separator + "Poleidoscope" + (SystemClock.uptimeMillis() % 10000) + ".gif";
                                            FileOutputStream outStream = new FileOutputStream(filename);
                                            outStream.write(bos.toByteArray());
                                            outStream.close();

                                            MediaScannerConnection.scanFile(MainActivity.self, new String[]{filename}, null, null );

                                            // Inform the user about the new GIF
                                            MainActivity.self.runOnUiThread(
                                                    new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            Snackbar snapshotMessage = Snackbar.make( MainActivity.sourceLayout, "GIF saved", Snackbar.LENGTH_LONG );
                                                            snapshotMessage.setAction("View", new View.OnClickListener() {
                                                                @Override
                                                                public void onClick(View v) {
                                                                    Intent intent = new Intent();
                                                                    intent.setAction(Intent.ACTION_VIEW);
                                                                    intent.setDataAndType(Uri.parse(filename), "image/*" );
                                                                    MainActivity.self.startActivity( intent );
                                                                }
                                                            });
                                                            snapshotMessage.show();
                                                            MainActivity.self.findViewById(R.id.snapshot).setVisibility(View.VISIBLE);
                                                        }
                                                    });
                                        } catch(Exception e) {
                                            e.printStackTrace();
                                            MainActivity.self.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.self);
                                                    dialogBuilder.setMessage( "There was an error saving the file!\n\n"+e.getMessage() );
                                                    dialogBuilder.setTitle("Error");
                                                    dialogBuilder.setNeutralButton("OK", null );
                                                    AlertDialog dialog = dialogBuilder.create();
                                                    dialog.show();
                                                }
                                            });
                                        }
                                        bos = null;
                                    }
                                };
                                MainActivity.cameraHandler.post( writeGifThread );
                                //writeGifThread.run();
                            }
                        }
                ).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d( "Save GIF", "Canceled." );
                        frames.clear();
                        framecount = 0;
                        timeStamps = null;
                    }
                }
                ).setView(seekBar);

                AlertDialog gifSaveDialog = gifSaveDialogBuilder.create();

                gifSaveDialog.show();
            }
        });
    }

    public void setCoordSys( CoordinateSystem c ){
        this.coordSys = c;
        return;
    }
}
