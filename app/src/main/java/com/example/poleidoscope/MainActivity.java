/*
 *     Poleidoscope
 *     Copyright (C) 2021  Matthias Zach
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.example.poleidoscope;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Matrix;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.navigation.fragment.NavHostFragment;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ActionMode;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.PopupMenu;
import android.widget.Toast;

import java.util.Vector;

import static com.example.poleidoscope.Nipple.BUTTON_SIZE;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    public static Context context;
    public static AppCompatActivity self;
    public static ConstraintLayout sourceLayout;
    public static NippleModView nippleModView;
    //public static SourceViewOpenGL sourceView;
    public static PreViewOpenGL sourceView;
    public static FloatingActionButton fab;

    public static CameraManager cameraManager;
    private final int CAMERA_ID_START = 254839;
    public static Handler cameraHandler;
    public static HandlerThread cameraThread;
    private boolean startup = true;
    private boolean recording = false;
    private long startRecordTime;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        self = this;
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        cameraManager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);

        if (nippleModView == null) {
            nippleModView = new NippleModView(this);
        }

        sourceLayout = findViewById(R.id.source_layout);
        if (sourceLayout == null) throw new NullPointerException("source_layout not found.");
        sourceLayout.addView(nippleModView);
        nippleModView.setVisibility(View.INVISIBLE);

        sourceView = sourceLayout.findViewById(R.id.source_view);
        if (sourceView == null) throw new NullPointerException("source_view not found.");
        sourceView.setPreserveEGLContextOnPause(true);


        startCameraThread();


        fab = findViewById(R.id.snapshot);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sourceView != null) {
                    // Take a picture of the current screen and store it.
                    takeSnapshot();
                }
            }
        });
        fab.setOnLongClickListener( new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                sourceView.startRecording();
                Snackbar snapshotMessage = Snackbar.make( sourceLayout, "start recording to GIF", Snackbar.LENGTH_LONG );
                snapshotMessage.show();
                recording = true;
                return true;
            }
        });
        fab.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch( event.getAction() ){
                    case MotionEvent.ACTION_DOWN:
                        startRecordTime = SystemClock.uptimeMillis();
                        sourceView.startRecording();
                        break;
                    case MotionEvent.ACTION_UP:
                        if( SystemClock.uptimeMillis() - startRecordTime < 500 ){
                            sourceView.takeSnapshot();
                        } else {
                            //sourceView.stopRecording();
                            sourceView.takeSnapshot();
                        }
                        break;
                }
                sourceView.requestRender();
                return(true);
            }
        });

        /*Toast.makeText(MainActivity.self, "Welcome to the Poleidoscope!\n\n" +
                "Touch the screen to start.", Toast.LENGTH_LONG).show();

         */
        showInformation();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.show_information:
                showInformation();
                break;
            case R.id.reset:
                reset();
                break;
            //case R.id.animate_graph:
            //    break;
            //case R.id.action_settings:

                //return true;
            /*case R.id.set_nipples:
                Nipple newNipple = setNewNipple();*/
                //startActionMode(newNipple.nippleModCallback);
                /*
                PopupMenu nippleMenu = new PopupMenu(this, findViewById(R.id.set_nipples) );
                nippleMenu.inflate( R.menu.set_menu );
                nippleMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        sourceView.setNipple( item.getItemId() );
                        Toast.makeText(MainActivity.self, "you can now set nipples", Toast.LENGTH_SHORT).show();
                        return true;
                    }
                });
                nippleMenu.show(); */
                //return true;
            case R.id.select_camera:
                PopupMenu cameraMenu = new PopupMenu(this, findViewById(R.id.select_camera));
                cameraMenu.inflate(R.menu.camera_menu);

                // Populate the select camera menu with the available cameras on the phone
                try {
                    String[] cameraIds = cameraManager.getCameraIdList();
                    int i;
                    for (i = 0; i < cameraIds.length; i++) {
                        // Add the selection of the camera to the menu
                        cameraMenu.getMenu().add(0, CAMERA_ID_START + i, i + 1, "Camera no. " + cameraIds[i]);
                    }
                } catch (CameraAccessException e) {
                    e.printStackTrace();
                }

                // Set the onClickListener on the corresponding methods
                cameraMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.camera_none) {
                            Toast.makeText(MainActivity.self, "no camera selected", Toast.LENGTH_SHORT).show();
                            sourceView.switchToCamera(PreViewOpenGL.NO_CAMERA);
                            return true;
                        } else {
                            Toast.makeText(MainActivity.self, "selected camera no. " + (item.getItemId() - CAMERA_ID_START), Toast.LENGTH_SHORT).show();
                            sourceView.switchToCamera(item.getItemId() - CAMERA_ID_START);
                            return true;
                        }
                    }
                });

                cameraMenu.show();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public Nipple setNewNipple() {
        CoordinateSystem coordSys = sourceView.getCoordSys();
        Nipple newNipple = new Nipple(this, coordSys, new ComplexDouble((coordSys.x0 + coordSys.x1) / 2, (coordSys.y0 + coordSys.y1) / 2));
        newNipple.type = Nipple.ROOT;
        nippleModView.addNipple(newNipple);
        sourceLayout.addView(newNipple);
        sourceView.invalidation();
        return newNipple;
    }


    /* A class to manage the display and modification
     * of all nipples indicating roots, poles, and
     * coefficients.
     */
    public class NippleModView extends View {

        private float x0, y0;
        private Vector<Nipple> nipples;

        public NippleModView(Context context) {
            super(context);
            ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams(
                    ConstraintLayout.LayoutParams.MATCH_PARENT,
                    ConstraintLayout.LayoutParams.MATCH_PARENT);
            super.setLayoutParams( params );

            setBackgroundColor( Color.RED );
            setAlpha(0.2f);

            nipples = new Vector<Nipple>();
            return;
        }

        public NippleModView(Context context, @Nullable AttributeSet attrs) {
            super(context, attrs);
        }

        public NippleModView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
        }

        @Override
        public boolean onTouchEvent( MotionEvent event) {

            switch( event.getAction() ) {
                case MotionEvent.ACTION_DOWN:
                    x0 = event.getX();
                    y0 = event.getY();
                    break;
                case MotionEvent.ACTION_MOVE:
                    if( Nipple.selectedNipple != null ) {
                        Nipple nipple = Nipple.selectedNipple;
                        float x = event.getX();
                        float y = event.getY();
                        nipple.setX(nipple.getX() + (x - x0));
                        nipple.setY(nipple.getY() + (y - y0));
                        nipple.updateFromScreenValues();
                        x0 = x;
                        y0 = y;
                    }
                    break;
                default:
                    break;
            }
            sourceView.invalidation();
            return true;
        }

        public void activate(){
            setVisibility( View.VISIBLE );
            return;
        }

        public void deactivate(){
            setVisibility( View.INVISIBLE );
            return;
        }

        public void addNipple( Nipple nipple ){
            nipples.add( nipple );
            return;
        }

        public void deleteNipple( Nipple nipple ){
            int i;
            for( i=0; i<nipples.size(); i++ ){
                if( nipples.get(i) == nipple ){
                    nipples.remove(i);
                }
            }
            sourceLayout.removeView( nipple );
            nipple.setVisibility( View.GONE );
            return;
        }

        public Polynomial prepareNumerator(){
            int i;
            Polynomial numerator = new Polynomial();
            Nipple nipple;
            for( i=0; i<nipples.size(); i++ ){
                nipple = nipples.get(i);
                if( nipple.type == Nipple.ROOT ){
                    numerator.addRoot(nipple.getValue());
                }
            }
            return numerator;
        }


        public Polynomial prepareDenominator(){
            int i;
            Polynomial denominator = new Polynomial();
            Nipple nipple;
            for( i=0; i<nipples.size(); i++ ){
                nipple = nipples.get(i);
                if( nipple.type == Nipple.POLE ){
                    denominator.addRoot(nipple.getValue());
                }
            }
            return denominator;
        }

        public ComplexDouble prepareLeadCoeff(){
            return new ComplexDouble(1.0);
        }

        public void updateNipples(){
            int i;
            for( i=0; i<nipples.size(); i++ ){
                Nipple nipple = nipples.get(i);
                nipple.updateFromCoordValues();
            }
        }

        public Nipple setNewNipple( float x, float y ) {
            CoordinateSystem coordSys = sourceView.getCoordSys();
            Nipple newNipple = new Nipple( context, coordSys, x, y );
            newNipple.type = Nipple.ROOT;
            nippleModView.addNipple(newNipple);
            sourceLayout.addView(newNipple);
            // if this is the very first nipple, turn on the camera
            Log.d( TAG, "Checking startup: " + startup );
            if( startup ){
                Log.d( TAG, "Setting very first nipple" );
                //long time = SystemClock.uptimeMillis();
                //while( SystemClock.uptimeMillis() < time + 1000 ){}
                //sourceView.requestTiling();
                sourceView.switchToCamera( 0 );
                startup = false;
            } else {
                sourceView.invalidation();
            }
            return newNipple;
        }

        public void deleteNipples() {
            int i;
            for (i = 0; i < nipples.size(); i++) {
                sourceLayout.removeView(nipples.get(i));
                nipples.get(i).setVisibility(View.GONE);
            }
            nipples.clear();
            return;
        }

    }

    private void startCameraThread(){
        cameraThread = new HandlerThread( "Camera background thread" );
        cameraThread.start();
        cameraHandler = new Handler( cameraThread.getLooper() );
        return;
    }

    private void stopCameraThread(){
        try {
            cameraThread.join();
            cameraThread = null;
            cameraHandler = null;
        } catch( InterruptedException e ){
            e.printStackTrace();
        }
        return;
    }

    /*
    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");

        startCameraThread();
    }
    @Override
    protected void onPause() {
        Log.e(TAG, "onPause");
        sourceView.switchToCamera( PreViewOpenGL.NO_CAMERA );
        //sourceView.onPause();

        stopCameraThread();
        super.onPause();

        //closeCamera();
        //stopBackgroundThread();
    }*/

    private void takeSnapshot(){
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(MainActivity.self, new String[]{ Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0 );
            Toast.makeText(MainActivity.self, "Error: No permission to store images.", Toast.LENGTH_LONG).show();
            return;
        }
        if( sourceView != null ){
            final String filename = sourceView.takeSnapshotOld();
            MediaScannerConnection.scanFile(this, new String[]{filename}, null, null );
            Snackbar snapshotMessage = Snackbar.make( sourceLayout, R.string.snapshot_taken, Snackbar.LENGTH_LONG );
            snapshotMessage.setAction("View", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse(filename), "image/*" );
                    startActivity( intent );
                }
            });
            snapshotMessage.show();
        } else {
            Log.e( TAG, "sourceView not available" );
            Toast.makeText(MainActivity.self, "Error: sourceView not found.", Toast.LENGTH_LONG).show();
        }
    }

    private void showInformation(){
        AlertDialog.Builder welcomeDialogBuilder = new AlertDialog.Builder(this );
        welcomeDialogBuilder.setMessage( R.string.info_text
                /* "This is a Kaleidoscope based on " +
                        "complex rational functions instead " +
                        "of mirrors. \n\n" +
                        "The screen shows a section of the complex " +
                        "plane and the function is defined by specifying " +
                        "its zeroes and poles. Clicking on an already set zero/pole " +
                        "switches to 'modification mode'.\n\n" +
                        "For a given function f, the Poleidoscope " +
                        "computes the 'preimage' of a graphic projected " +
                        "into the target complex plane: The " +
                        "color of a pixel p on the screen at the " +
                        "complex number z is determined by the color of " +
                        "the graphic at f(z).\n\n" +
                        "Select one of the phone's cameras " +
                        "to take a look through the Poleidoscope " +
                        "and use the life camera picture as graphic.\n\n" +
                        "Your snapshots will be saved to your " +
                        "local 'Pictures' directory."*/
        ).setTitle(
                                "Welcome to the Poleidoscope!"
        ).setNeutralButton("OK", null );

        AlertDialog welcomeDialog = welcomeDialogBuilder.create();

        welcomeDialog.show();
    }

    private void reset(){

        nippleModView.deleteNipples();
        nippleModView.deactivate();
        sourceView.switchToCamera(PreViewOpenGL.NO_CAMERA);
        sourceView.invalidation();
        sourceView.reset();
        startup = true;
    }

}
